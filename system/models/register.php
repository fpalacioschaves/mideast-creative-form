<?php

class register {
    /*     * *
     * Get All Normal Registered items
     * @access	public
     * @return	mixed
     */

    public function getNormalRegister() {

        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_table_teams";

        $query = $wpdb->get_results("SELECT * FROM " . $reg_method_3);

        return $query;
    }

    public function getTeamMembers($team_id) {

        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_table_team_members";

        $query = $wpdb->get_results("SELECT * FROM " . $reg_method_3 . " WHERE id_team = " . $team_id);

        return $query;
    }

    /*     * *
     * Get All Payment Registered items
     * @access	public
     * @return	mixed
     */

    public function getPaymentRegister() {

        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_table_teams";

        $query = $wpdb->get_results("SELECT * FROM " . $reg_method_3);

        return $query;
    }

    /**
     * Marc Item as readed/unreaded
     * @param $type
     * @param $id
     * @param null $value_data
     * @return mixed
     */
    public function markItemAsReaded($type, $id, $value_data = null) {
        global $wpdb;

        if ($type == 'normal') {
            $reg_method = $wpdb->prefix . "reg_table_teams";
        }

        if ($type == 'payment') {
            $reg_method = $wpdb->prefix . "reg_method_4";
        }

        if ($value_data) {

            $val = 0;
            if ($value_data == 'success') {
                $val = 0;
            } elseif ($value_data == 'info') {
                $val = 1;
            }

            $data = $wpdb->update(
                    $reg_method, array('viewed' => $val), array('id' => $id), array('%d'), array('%d')
            );
        } else {

            $data = $wpdb->update(
                    $reg_method, array('viewed' => 1), array('id' => $id), array('%d'), array('%d')
            );
        }

        return $data;
    }

    /**
     * Remove Item
     * @param $type
     * @param $item_id
     * @return mixed
     */
    public function removeItem($type, $item_id) {

        global $wpdb;

        if ($type == 'normal') {

            $reg_method = $wpdb->prefix . "reg_table_teams";
            $reg_method2 = $wpdb->prefix . "reg_table_team_members";
        }



        $result = $wpdb->delete($reg_method, array('id' => $item_id));
        $result2 = $wpdb->delete($reg_method2, array('id_team' => $item_id));
        return $result;
    }

    /**
     * Get Register Item
     * @param $type
     * @param $id
     * @return mixed
     */
    public function getRegisterItem($type, $id) {
        global $wpdb;

        if ($type == 'normal') {

            $reg_method = $wpdb->prefix . "reg_table_teams";
        }

        if ($type == 'payment') {
            $reg_method = $wpdb->prefix . "reg_method_4";
        }

        $result = $wpdb->get_row(
                "
                                            SELECT *
                                            FROM " . $reg_method . "
                                            WHERE id = '" . $id . "'
                                         "
        );

        return $result;
    }

    /**
     * Count Registers
     * @param $type
     * @return int
     */
    public function countRegisters($type) {

        global $wpdb;

        if ($type == 'normal') {
            $reg_method = $wpdb->prefix . "reg_table_teams";
        }

        if ($type == 'payment') {
            $reg_method = $wpdb->prefix . "reg_method_4";
        }

        $query = $wpdb->get_results("SELECT * FROM " . $reg_method . " WHERE viewed='0'");

        return count($query);
    }

    /**
     * Add Normal Register Admin
     * @param $data
     * @return mixed
     */
    public function addNormalRegisterItem($data) {
        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_table_teams";

        $query = $wpdb->insert(
                $reg_method_3, array(
            'team_name' => sanitize_text_field($data['full_name']),
            'idea_description' => sanitize_text_field($data['idea_description']),
            'how_far' => sanitize_text_field($data['how_far']),
            'problem_to_solve' => sanitize_text_field($data['problem_to_solve']),
            'how_idea_solve' => sanitize_text_field($data['how_idea_solve']),
            'target_customer' => sanitize_text_field($data['target_customer']),
            'experience' => sanitize_text_field($data['experience']),
            'motivation' => sanitize_text_field($data['motivation']),
            'hope_to_gain' => sanitize_text_field($data['hope_to_gain']),
            'other_participations' => sanitize_text_field($data['other_participations']),
            'wich_participations' => sanitize_text_field($data['wich_participations']),
            //'run_start_up' => sanitize_text_field($data['run_start_up']),
            //'how_many_start_up' => sanitize_text_field($data['how_many_start_up']),
            'current_start_up' => sanitize_text_field($data['current_start_up']),
            'name_your_business' => sanitize_text_field($data['name_your_business']),
            'founder_cofounder' => sanitize_text_field($data['founder_cofounder']),
                ), array(
            //'%s',
            //'%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
                )
        );
        //return $query;




        return $query;
    }

    public function addNormalRegisterMemberCoreItem($data) {
        $get = $_GET;
        if (isset($data['number_members'])) {
            $number_members = $data['number_members'];
        } else {
            $number_members = 1;
        }

        if (isset($get) && isset($get['team_id']) && $get['team_id'] != "") {
            $team_id = $get['team_id'];
        } else {
            global $wpdb;
            $reg_method_4 = $wpdb->prefix . "reg_table_teams";
            $team = $wpdb->get_row("SELECT * FROM $reg_method_4 ORDER BY id DESC");
            $team_id = $team->id;
        }
        $reg_method_3 = $wpdb->prefix . "reg_table_team_members";
//        print_r($data);
//        wp_die();
//        wp_die($number_members." ".$team_id);
        if (isset($data['language_skills'])) {
            $language_skills = implode(",", $data['language_skills']);
        } else {
            $language_skills = "";
        }
        $query = $wpdb->insert(
                $reg_method_3, array(
            'id_team' => $team_id,
            'full_name' => sanitize_text_field($data['full_name']),
            'birth_date' => sanitize_text_field($data['birthdate']),
            'address' => sanitize_text_field($data['address']),
            'phone_number' => sanitize_text_field($data['phone_number']),
            'email' => sanitize_text_field($data['email']),
            'educational_background' => sanitize_text_field($data['educational_background']),
            'current_position' => sanitize_text_field($data['current_job']),
            'twitter_profile' => sanitize_text_field($data['twitter_profile']),
            'facebook_profile' => sanitize_text_field($data['facebook_profile']),
            'language_skills' => $language_skills,
            'other_language' => sanitize_text_field($data['other_language']),
            'member_type' => "Head",
                ), array(
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
            '%s',
                )
        );


        return $query;
    }

    public function addNormalRegisterMemberItem($data) {
        $get = $_GET;
        if (isset($data['number_members'])) {
            $number_members = $data['number_members'];
        } else {
            $number_members = 1;
        }

        if (isset($get) && isset($get['team_id']) && $get['team_id'] != "") {
            $team_id = $get['team_id'];
        } else {
            global $wpdb;
            $reg_method_4 = $wpdb->prefix . "reg_table_teams";
            $team = $wpdb->get_row("SELECT * FROM $reg_method_4 ORDER BY id DESC");
            $team_id = $team->id;
        }
        $reg_method_3 = $wpdb->prefix . "reg_table_team_members";

        if ($number_members > 1) {
            for ($i = 1; $i < $number_members; $i++) {

                $full_name = "full_name" . $i;
                $birth_date = "birthdate" . $i;
                $email = "email" . $i;
                $educational_background = "educational_background" . $i;
                $current_position = "current_position" . $i;
                $twitter_profile = "twitter_profile" . $i;
                $facebook_profile = "facebook_profile" . $i;
                $language_skills = "language_skills" . $i;
                $other_language = "other_language" . $i;
                $member_type = "member_type" . $i;


                if (isset($data['language_skills'])) {
                    $language_skills_value = implode(",", $data[$language_skills]);
                } else {
                    $language_skills_value = "";
                }

                //$language_skills_value = implode(",", $data[$language_skills]);





                $query = $wpdb->insert(
                        $reg_method_3, array(
                    'id_team' => $team_id,
                    'full_name' => sanitize_text_field($data[$full_name]),
                    'birth_date' => sanitize_text_field($data[$birth_date]),
                    'email' => sanitize_text_field($data[$email]),
                    'educational_background' => sanitize_text_field($data[$educational_background]),
                    'current_position' => sanitize_text_field($data[$current_position]),
                    'twitter_profile' => sanitize_text_field($data[$twitter_profile]),
                    'facebook_profile' => sanitize_text_field($data[$facebook_profile]),
                    'language_skills' => $language_skills_value,
                    'other_language' => sanitize_text_field($data[$other_language]),
                    'member_type' => sanitize_text_field($data[$member_type]),
                        ), array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
//                        '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                        )
                );
            }
        }
        return $query;
    }

    public function update_normal_form($form_elements) {
        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_table_teams";


        $query = $wpdb->update(
                $reg_method_3, array(
            'team_name' => sanitize_text_field($form_elements['team_name']),
            'idea_description' => sanitize_text_field($form_elements['idea_description']),
            'how_far' => sanitize_text_field($form_elements['how_far']),
            'problem_to_solve' => sanitize_text_field($form_elements['problem_to_solve']),
            'how_idea_solve' => sanitize_text_field($form_elements['how_idea_solve']),
            'target_customer' => sanitize_text_field($form_elements['target_customer']),
            'experience' => sanitize_text_field($form_elements['experience']),
            'motivation' => sanitize_text_field($form_elements['motivation']),
            'hope_to_gain' => sanitize_text_field($form_elements['hope_to_gain']),
            'other_participations' => sanitize_text_field($form_elements['other_participations']),
            'wich_participations' => sanitize_text_field($form_elements['wich_participations']),
            'run_start_up' => sanitize_text_field($form_elements['run_start_up']),
            'how_many_start_up' => sanitize_text_field($form_elements['how_many_start_up']),
            'current_start_up' => sanitize_text_field($form_elements['current_start_up']),
            'name_your_business' => sanitize_text_field($form_elements['name_your_business']),
            'founder_cofounder' => sanitize_text_field($form_elements['founder_cofounder']),
//                                                'your_name_3'                   => isset($form_elements['your_name_3']) ? $form_elements['your_name_3'] : '',
//                                                'your_fam_name_3'               => isset($form_elements['your_fam_name_3']) ? $form_elements['your_fam_name_3'] : '' ,
//                                                'your_email_3'                  => isset($form_elements['your_email_3']) ? $form_elements['your_email_3'] : '',
//                                                'mobile_phone_3'                => isset($form_elements['mobile_phone_3']) ? $form_elements['mobile_phone_3'] : '',
//                                                'nationality_3'                 => isset($form_elements['nationality_3']) ? $form_elements['mobile_phone_3'] : '',
//                                                'affiliated_org_3'              => isset($form_elements['affiliated_org_3']) ? $form_elements['affiliated_org_3'] : '',
//                                                'dietary_requirements_3'        => isset($form_elements['dietary_requirements_3']) ? serialize($form_elements['dietary_requirements_3']) : '',
//                                                'other_namely_3'                => isset($form_elements['other_namely_3']) ? $form_elements['other_namely_3']: '',
//                                                'food_allergies_3'              => isset($form_elements['food_allergies_3']) ? $form_elements['food_allergies_3'] : '',
//                                                'special_assistance_3'          => isset($form_elements['special_assistance_3']) ? $form_elements['special_assistance_3'] : '',
//                                                'gender_option_3'               => isset($form_elements['genderOption3']) ? $form_elements['genderOption3'] : '',
//                                                'medical_condition_3'           => isset($form_elements['medical_condition_3']) ? $form_elements['medical_condition_3'] : '',
//                                                'hear_from_this_event_3'        => isset($form_elements['hearFromThisEvent3']) ? serialize($form_elements['hearFromThisEvent3']) : '',
//                                                'other_3'                       => isset($form_elements['other_3']) ? $form_elements['other_3'] : '',
//                                                'date'                          => time(),
                ), array('id' => $form_elements['item_id_3'])
//                                            array(
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                                '%s',
//                                            ),
//                                            array( '%d' )
        );

        return $query;
    }

    public function checkNormalEmailRegister($email) {
        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_method_3";

        $result = $wpdb->get_results(
                "
                                                            SELECT your_email_3
                                                            FROM " . $reg_method_3 . "
                                                            WHERE your_email_3 = '" . $email . "'
                                                         "
        );

        return $result;
    }

    /**
     * Check Payment Registered email
     * @param $email
     * @return mixed
     */
    public function checkPaymentEmailRegister($email) {

        global $wpdb;

        $reg_method_3 = $wpdb->prefix . "reg_method_4";

        $result = $wpdb->get_results(
                "
                                                SELECT your_email_4
                                                FROM " . $reg_method_3 . "
                                                WHERE your_email_4 = '" . $email . "'
                                                "
        );

        return $result;
    }

}
