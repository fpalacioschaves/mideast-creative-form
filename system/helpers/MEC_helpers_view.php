<?php
//Check if helper class DEB_helpers_view exist
if(!class_exists('MEC_helpers_view'))
{
        class MEC_helpers_view
        {
                /***
                 *
                 * This function allow to load php file as template view inside of a function.
                 * This function takes two parameters:
                 * $file: path to the included template file
                 * $data(optional) : variables to be included into template file from the function from where was called the template.
                 *
                 * Example1: $this->view->loadView("/views/menu/MEC_views_menu") ;
                 * Example2:
                 *
                 * $data1= 'val1';
                 * $data2= 'val2';
                 *
                 * @access	public
                 * @param	string
                 * @param	array
                 * @param	string
                 * @return	string
                 */
                public function loadView($file,$data=null, $function='include')
                {

                    if($data)
                    {
                        if(array($data))
                        {
                            extract($data) ;
                        }
                    }
                    if(is_file(MEC_PATH .'/'. $file.".php" ))
                    {
                        $filename = MEC_PATH .'/'. $file.".php" ;

                        ob_start();
                            if($function=='require')
                            {
                                require $filename;
                            }
                            elseif($function=='require_once')
                            {
                                require_once $filename;
                            }
                            elseif($function=='include_once')
                            {
                                include_once $filename;
                            }
                            else
                            {
                                include $filename;
                            }
                         echo ob_get_clean();

                    }
                    else
                    {
                        wp_die("ERROR: Requested ". MEC_PATH .'/'. $file.".php" ." file does not exist") ;
                    }

                }

                /**
                 * Write a log data
                 * @access public
                 * @param $data
                 */
                public function logData($data)
                {
                    $fp = fopen(MEC_PATH. '/system/log/logdata', 'a');
                    fwrite($fp, "--- ".$_SERVER['HTTP_USER_AGENT']. " - IP= (".$_SERVER['REMOTE_ADDR']." )".$data . ' - ' .date("d-m-Y H:i:s"). "\n");
                    fclose($fp);
                }


            public function sendEmail($from,$to, $subject, $body)
            {


                //$headers = "From: " . strip_tags(get_option('_nrRegEm')) . "\r\n";
               $headers = "From: Disrupt!/Media!/ - Kevin Coyne <" . strip_tags($from) . ">\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                return mail($to, $subject, $body, $headers) ;
            }
        }
}
