<?php
//The data type of those fieds are not finished yet depends on what else clients want to add.
//TODO
function MEC_create_table()
{
    global $wpdb;

    
    $reg_table_teams      = $wpdb->prefix . "reg_table_teams";
    $reg_table_team_members      = $wpdb->prefix . "reg_table_team_members";
     
    $sql1 = "

CREATE TABLE IF NOT EXISTS `".$reg_table_team_members."` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_team` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `birth_date` varchar(100) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `educational_background` text NOT NULL,
  `current_position` varchar(50) NOT NULL,
  `twitter_profile` varchar(100) NOT NULL,
   `facebook_profile` varchar(100) NOT NULL,
  `language_skills` text NOT NULL,
  `other_language` varchar(100) NOT NULL,
  `member_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

";

$sql2 = "

CREATE TABLE IF NOT EXISTS `".$reg_table_teams."` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(100) NOT NULL,
  `idea_description` text NOT NULL,
  `how_far` text NOT NULL,
  `problem_to_solve` text NOT NULL,
  `how_idea_solve` text NOT NULL,
  `target_customer` text NOT NULL,
  `experience` text NOT NULL,
  `motivation` text NOT NULL,
  `hope_to_gain` text NOT NULL,
  `other_participations` varchar(10) NOT NULL,
  `wich_participations` text NOT NULL,
  `run_start_up` varchar(10) NOT NULL,
  `how_many_start_up` varchar(100) NOT NULL,
  `current_start_up` varchar(10) NOT NULL,
  `name_your_business` varchar(100) NOT NULL,
  `founder_cofounder` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

";



    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql1 );
    dbDelta( $sql2 );


}

