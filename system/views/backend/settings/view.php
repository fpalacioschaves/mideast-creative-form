<script type="text/javascript">

    jQuery(document).ready(function ($) {

        $save_button = $("#saveSettings")

        $save_button.click(function () {

        var data = {
                        action: 'save_settings',
                       // payMode: $("#payMode").val(),
                       // payCurrency: $("#payCurrency").val(),
                       // payCulture: $("#payCulture").val(),
                      //  payAccId: $("#payAccId").val(),
                      //  paySiteId: $("#paySiteId").val(),
                      //  paySiteCode: $("#paySiteCode").val(),
                        nrRegEm: $("#nrRegEm").val()
                    };

        $save_button.attr("disabled", "disabled");

        $.post('/wp-admin/admin-ajax.php', data, function (response) {
            //alert("success");
        })
        .done(function (response) {
            //alert("second success");
        })
        .fail(function (response) {
            //alert("error");
        })
        .always(function (response) {

            //alert("finished");
            $save_button.removeAttr("disabled");

                $("#infoResult").html(response);

        });
        });

    });

</script>
<div class="wrap">

    <div id="icon-options-general" class="icon32"><br></div>
    <h2><?=$page_title;?></h2>

    <p id="infoResult"></p>
    <table class="form-table">
    <tbody>

<!--    <tr valign="top">
        <th scope="row"><label for="payMode">Multisafepay Payment Mode</label></th>
        <td>
            <select name="payMode" id="payMode">

                <option value="true" <?php echo  get_option( '_payMode') == "true"? ' selected ' : '  '  ?>>Test</option>

                <option value="false" <?php echo  get_option( '_payMode') == "false"? ' selected ' : '  '  ?>>Live</option>

            </select>
        </td>
    </tr>-->

<!--    <tr valign="top">
        <th scope="row"><label for="payCurrency">Multisafepay Currency</label></th>
        <td>
            <select name="payCurrency" id="payCurrency">
                <option value="EUR" <?php echo  get_option( '_payCurrency') == "EUR"? ' selected ' : '  '  ?>>€</option>
                <option value="USD" <?php echo  get_option( '_payCurrency') == "USD"? ' selected ' : '  '  ?>>$</option>
                <option value="GBP" <?php echo  get_option( '_payCurrency') == "GBP"? ' selected ' : '  '  ?>>£</option>
            </select>
            <p class="description">The currency code (e.g. EUR, USD, GBP). Make sure the specified payment method supports the specified currency.</p>
        </td>
    </tr>-->

<!--  <tr valign="top">
        <th scope="row"><label for="payCulture">Multisafepay ISO Locale code</label></th>
        <td>
            <select name="payCulture" id="payCulture">
                <option value="en" <?php echo  get_option( '_payCulture') == "en"? ' selected ' : '  '  ?>>EN</option>
                <option value="de" <?php echo  get_option( '_payCulture') == "de"? ' selected ' : '  '  ?>>DE</option>
                <option value="nl" <?php echo  get_option( '_payCulture') == "nl"? ' selected ' : '  '  ?>>NL</option>
            </select>
            <p class="description">Language (ISO 639) and Country (ISO 3166)</p>
        </td>
    </tr>-->

<!--    <tr valign="top">
        <th scope="row"><label for="payAccId">Multisafepay Account ID</label></th>
        <td><input name="payAccId" type="text" id="payAccId" value="<?php echo  get_option( '_payAccId') ; ?>" class="regular-text">
            <p class="description">Merchant Account ID</p></td>
    </tr>-->

<!--    <tr valign="top">
        <th scope="row"><label for="paySiteId">Multisafepay Website ID</label></th>
        <td><input name="paySiteId" type="text" id="paySiteId" value="<?php echo  get_option( '_paySiteId') ; ?>" class="regular-text">
            <p class="description">Merchant Site ID</p></td>
    </tr>-->

<!--


    <tr valign="top">
        <th scope="row"><label for="paySiteCode">Multisafepay Website Code</label></th>
        <td><input name="paySiteCode" type="text" id="paySiteCode" value="<?php echo   get_option( '_paySiteCode') ; ?>" class="regular-text">
            <p class="description">Site Secure ID</p></td>
    </tr>-->








    <tr valign="top">
        <th scope="row"><label for="nrRegEm">Register inform email</label></th>
        <td><input name="nrRegEm" type="text" id="nrRegEm" value="<?php echo  get_option( '_nrRegEm') ; ?>" class="regular-text">
            <p class="description">The email where register item alert will be send.</p></td>
    </tr>


    </tbody>
    </table>
    <p class="submit"><input type="button" name="saveSettings" id="saveSettings" class="button button-primary" value="Save Changes"></p>

</div>