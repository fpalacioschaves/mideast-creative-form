<script type="text/javascript">
    jQuery(document).ready(function($) {


        $trash_button = $( "[name='remItem']" ) ;

        $trash_button.click(function()
        {

                if (confirm('Are you sure you want to Remove this item item?')) {
                    var itm_id  =  $(this).attr("itm-id") ;

                    var data    = {
                                        action: 'remove_payment_reg_item',
                                        item_id: itm_id
                                    };
                    $.post('admin-ajax.php', data, function(response)
                    {
                        if(response=='OK')
                        {
                            $("#tr-" + itm_id).fadeOut();
                        }
                    });
                }
        });

        $view_button = $( "[name='viewItem']" ) ;

        $view_button.click(function()
        {
            var $this       = $(this) ;

            var itm_id2     =  $(this).attr("itm-id2") ;

            var value_data1 =  $(this).attr("value_data1") ;

            var data = {
                            action: 'mark_payment_reg_item',
                            item_id: itm_id2,
                            value_data: value_data1
                        };
            $.post('admin-ajax.php', data, function(response)
            {
                if(response=='success')
                {
                    $this.attr("value_data1",'info');
                    $("#tr-" + itm_id2).removeClass('success');
                    $("#tr-" + itm_id2).addClass('info');
                    $this.removeClass('icon-eye-open');
                    $this.addClass('icon-eye-close');
                }
                else if(response=='info')
                {
                    $this.attr("value_data1",'success');
                    $("#tr-" + itm_id2).removeClass('info');
                    $("#tr-" + itm_id2).addClass('success');
                    $this.removeClass('icon-eye-close');
                    $this.addClass('icon-eye-open');
                }
            });
        });


        $("#ApplyBtn").click(function(){

            if (confirm('Are you sure you want to Remove all selected items?')) {
                if($("#normalFormAction").val() == '0')
                {
                    var boxes = $(":checkbox:checked");

                    boxes.each(function(){

                        var id = $(this).val() ;
                        var data = {
                            action: 'remove_payment_reg_item',
                            item_id: id
                        };
                        $.post('admin-ajax.php', data, function(response)
                        {
                            if(response=='OK')
                            {
                                $("#tr-" + id).fadeOut();
                            }
                        });
                    });
                }
            }

        });




        var $export_button  =    $("#export") ;
        function ajax_export_req()
        {
            var data = {
                action: 'export_payment_item',
                edit_item: 'all'
            };

            $export_button.attr("disabled", "disabled");

            $.post('/wp-admin/admin-ajax.php', data, function(response)
            {
                $export_button.removeAttr("disabled");
                document.location = response;
            });
        }

        $export_button.click(function(){
            ajax_export_req();
        });











    });
</script>

<div class="container-fluid">

    <div id="icon-themes" class="icon32"></div>
    <h2><?=$page_title;?></h2>

    <table class="table table-hover">

        <thead>
        <tr>
            <th>#RegId</th>
            <th>Name</th>
            <th>Email</th>
            <th>Nationality</th>
            <th>Mobile Phone</th>
            <th>Payment Option</th>
            <th>Total</th>
            <th>Payment Status</th>
            <th>Payment Transaction ID</th>
            <th>Date</th>
            <th>Action</th>
            <th class="pull-right">Mark</th>
        </tr>
        </thead>

        <tbody>

        <?php foreach($normal_register_list as $list): ?>
            <tr class="<?php echo $list->viewed ? ' success ' : ' info ' ; ?>" id="tr-<?php  echo $list->id; ?>">
                <td><p class="text-info"><i class="icon-th-list"></i> <?php  echo $list->id; ?></p></td>
                <td><a href="admin.php?page=register-payment&view_payment=<?php  echo $list->id; ?>"><?php echo $list->your_name_4; ?></a></td>
                <td><p class="text-info"><?php echo $list->your_email_4; ?></p></td>
                <td><p class="text-info"><?php echo $list->input_nationality_4; ?></p></td>
                <td><p class="text-info"><?php echo $list->mobile_phone_4; ?></p></td>
                <td><p class="text-info"><?php echo $list->payment_option ;?></p></td>
                <td><p class="text-info"><?php echo $list->total ;?> €</p></td>
                <td><p class="text-info"><?php echo $list->payment_status ;?></p></td>
                <td><p class="text-info"><?php echo $list->payment_transaction_id ;?></p></td>
                <td><p class="text-info"><?php echo date('d-m-Y H:i:s',$list->date); ?></p></td>
                <td>
                    <p>

                        <span class="pull-left"><a href="#" data-toggle="tooltip" data-placement="top" title="Remove Item" class="icon-trash" name="remItem" itm-id="<?php  echo $list->id; ?>"></a></span>
                        <span class="pull-right"><a href="#" data-toggle="tooltip" value_data1='<?php echo $list->viewed ? 'success' : 'info' ; ?>' title="<?php echo $list->viewed ? ' Mark item as unread it. ' : '  Mark item as read it. ' ; ?>" itm-id2="<?php  echo $list->id; ?>"  name="viewItem" data-placement="top" class="<?php echo $list->viewed ? ' icon-eye-open ' : '  icon-eye-close ' ; ?>" id="view-<?php  echo $list->id; ?>"></a></span>

                    </p>

                </td>
                <td><input type="checkbox" class="pull-right" name="mark[]" value="<?php  echo $list->id; ?>"></td>

            </tr>
        <?php endforeach; ?>

        </tbody>

    </table>

    <div class="form-inline">

        <select id="normalFormAction">
            <option value="">---</option>
            <option value="0">Remove</option>
        </select>

        <input type="button" class="btn" id="ApplyBtn" name="newCodeBtn" value="Apply">
        <input type="button" id="export" class="btn" value="Export All" >
    </div>

</div>


