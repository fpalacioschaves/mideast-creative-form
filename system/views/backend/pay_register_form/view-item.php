<script type="text/javascript">
    jQuery(document).ready(function($) {

        var $create_button      =    $("#update_payment") ;
        var $form               =    $("#update-item-view") ;

        $create_button.click(function()
        {
            var data = {
                action: 'update_payment',
                data_to_send: $form.serialize()
            };

            $create_button.attr("disabled", "disabled");

            $.post('/wp-admin/admin-ajax.php', data, function(response)
            {
                $create_button.removeAttr("disabled");
                $("#info-show").html(response);
                $("#info-show2").html(response);

            });
        });
    });




</script>
<form id="update-item-view" name="update-item-view" method="post">
<p id="info-show"></p>
<div id="form-container">
<input type="hidden" name="item_id" value="<?php echo isset($_GET['view_payment']) ?  $_GET['view_payment'] : '' ;?>">

<div class="control-group">
        <legend>Personal information</legend>

        <p>
            <label for="request_choosed_option">Event type:</label>
            <p>
    <select id="eventType" name="eventType">
        <option <?php echo $get_register_item->request_choosed_option=='' ? ' selected ' : '' ; ?> value="">---</option>
        <option <?php echo $get_register_item->request_choosed_option=='2 Day Event' ? ' selected ' : '' ; ?> value="2 Day Event">2 Day Event</option>
        <option <?php echo $get_register_item->request_choosed_option=='Ignite + 2 Day Event' ? ' selected ' : '' ; ?> value="Ignite + 2 Day Event">Ignite + 2 Day Event</option>

    </select>

    </p>
        </p>

        <p>
            <label for="your_name_4">Your name.</label>
            <input id="your_name_4" class="input-xxlarge" name="your_name_4" value="<?php echo $get_register_item->your_name_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="your_fam_name_4">Family name.</label>
            <input id="your_fam_name_4" class="input-xxlarge" name="your_fam_name_4" value="<?php echo $get_register_item->your_fam_name_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="your_email_4">Your email</label>
            <input id="your_email_4" class="input-xxlarge" name="your_email_4" value="<?php echo $get_register_item->your_email_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="mobile_phone_4">Your mobile phone</label>
            <input id="mobile_phone_4" class="input-xxlarge" name="mobile_phone_4" value="<?php echo $get_register_item->mobile_phone_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_street_4">Your street</label>
            <input id="input_street_4" class="input-xxlarge" name="input_street_4" value="<?php echo $get_register_item->input_street_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_city_4">Your city</label>
            <input id="input_city_4" class="input-xxlarge" name="input_city_4" value="<?php echo $get_register_item->input_city_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_zip_code_4">Your ZIP code</label>
            <input id="input_zip_code_4" class="input-xxlarge" name="input_zip_code_4" value="<?php echo $get_register_item->input_zip_code_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_country_4">Your country</label>
            <input id="input_country_4" class="input-xxlarge" name="input_country_4" value="<?php echo $get_register_item->input_country_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_nationality_4">Your nationality</label>
            <input id="input_nationality_4" class="input-xxlarge" name="input_nationality_4" value="<?php echo $get_register_item->input_nationality_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_affiliated_organization_4">Affiliated with organization</label>
            <input id="input_affiliated_organization_4" class="input-xxlarge" name="input_affiliated_organization_4" value="<?php echo $get_register_item->input_affiliated_organization_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_org_street_4">Organization street</label>
            <input id="input_org_street_4" class="input-xxlarge" name="input_org_street_4" value="<?php echo $get_register_item->input_org_street_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_org_zip_code_4">Organization ZIP code</label>
            <input id="input_org_zip_code_4" class="input-xxlarge" name="input_org_zip_code_4" value="<?php echo $get_register_item->input_org_zip_code_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_org_city_4">Organization city</label>
            <input id="input_org_city_4" class="input-xxlarge" name="input_org_city_4" value="<?php echo $get_register_item->input_org_city_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_org_country_4">Organization country</label>
            <input id="input_org_country_4" class="input-xxlarge" name="input_org_country_4" value="<?php echo $get_register_item->input_org_country_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="input_org_mobile_phone_4">Organization mobile phone</label>
            <input id="input_org_mobile_phone_4" class="input-xxlarge" name="input_org_mobile_phone_4" value="<?php echo $get_register_item->input_org_mobile_phone_4 ; ?>" type="text"/>
        </p>

        <p>
            <label for="case_emergency_name_4">Emergency name</label>
            <input id="case_emergency_name_4" class="input-xxlarge" name="case_emergency_name_4" value="<?php echo $get_register_item->case_emergency_name_4 ; ?>" type="text"/>
        </p>


        <p>
            <label for="case_emergency_relationship_4">Relationship / Affiliation</label>
            <input id="case_emergency_relationship_4" class="input-xxlarge" name="case_emergency_relationship_4" value="<?php echo $get_register_item->case_emergency_relationship_4 ; ?>" type="text"/>
        </p>

         <p>
            <label for="case_emergency_phone_4">Emergency phone</label>
            <input id="case_emergency_phone_4" class="input-xxlarge" name="case_emergency_phone_4" value="<?php echo $get_register_item->case_emergency_phone_4 ; ?>" type="text"/>
        </p>
</div>





        <?php
        $values                 =array("I was invited by Hivos","via a colleague or contact of mine","via the Hivos website");

        $hear_from_this_event   = unserialize($get_register_item->hear_from_this_event) ;
        ?>

        <div class="control-group">

            <legend>Where did you hear from this event?</legend>

            <label class="checkbox" for="hearFromThisEvent4-1">
                <input type="checkbox" id="hearFromThisEvent4-1" name="hearFromThisEvent4[1]" <?php echo in_array($values[0], $hear_from_this_event)  ? ' checked ': '' ; ?> value="I was invited by Hivos">  I was invited by Hivos
            </label>

            <label class="checkbox" for="hearFromThisEvent4-2">
                <input type="checkbox" id="hearFromThisEvent4-2" name="hearFromThisEvent4[2]"  <?php echo  in_array($values[1], $hear_from_this_event) ? ' checked ': '' ; ?>  value="via a colleague or contact of mine">  via a colleague or contact of mine
            </label>

            <label class="checkbox" for="hearFromThisEvent4-3">
                <input type="checkbox" id="hearFromThisEvent4-3" name="hearFromThisEvent4[3]" <?php echo  in_array($values[2], $hear_from_this_event)  ? ' checked ': '' ; ?> value="via the Hivos website">
                 via the Hivos website
            </label>

            <p>
                <label for="input_hear_from_this_event_other_4">Other</label>
                <input id="input_hear_from_this_event_other_4" class="input-xxlarge" name="input_hear_from_this_event_other_4" value="<?php echo $get_register_item->input_hear_from_this_event_other_4 ; ?>" type="text"/>
            </p>

        </div>

        <?php
        $dietary_requirements                   =array("vegetarian","pescatarian","vegan","helal","kosjer","glutenfree");

        $dietary_requirements_4                 = unserialize($get_register_item->dietary_requirements_4) ;
        ?>

    <div class="control-group">

            <legend>Dietary wishes and medical conditions</legend>
            <p>
                <label class="checkbox" for="dietary_requirements_4-1">
                    <input type="checkbox" id="dietary_requirements_4-1" name="dietary_requirements_4[1]" <?php echo in_array($dietary_requirements[0], $dietary_requirements_4)  ? ' checked ': '' ; ?> value="vegetarian">  vegetarian
                </label>
            </p>
            <p>
                <label class="checkbox" for="dietary_requirements_4-2">
                    <input type="checkbox" id="dietary_requirements_4-2" name="dietary_requirements_4[2]"  <?php echo  in_array($dietary_requirements[1], $dietary_requirements_4) ? ' checked ': '' ; ?>  value="pescatarian">  pescatarian
                </label>
            </p>
            <p>
                <label class="checkbox" for="dietary_requirements_4-3">
                    <input type="checkbox" id="dietary_requirements_4-3" name="dietary_requirements_4[3]" <?php echo  in_array($dietary_requirements[2], $dietary_requirements_4)  ? ' checked ': '' ; ?> value="vegan"> vegan
                </label>
            </p>
            <p>
                <label class="checkbox" for="dietary_requirements_4-4">
                    <input type="checkbox" id="dietary_requirements_4-4" name="dietary_requirements_4[4]" <?php echo  in_array($dietary_requirements[3], $dietary_requirements_4)  ? ' checked ': '' ; ?> value="helal"> helal
                </label>
            </p>
            <p>
                <label class="checkbox" for="dietary_requirements_4-5">
                    <input type="checkbox" id="dietary_requirements_4-5" name="dietary_requirements_4[5]" <?php echo  in_array($dietary_requirements[4], $dietary_requirements_4)  ? ' checked ': '' ; ?> value="kosjer"> kosjer
                </label>
            </p>

            <p>
                <label class="checkbox" for="dietary_requirements_4-6">
                    <input type="checkbox" id="dietary_requirements_4-6" name="dietary_requirements_4[6]" <?php echo  in_array($dietary_requirements[5], $dietary_requirements_4)  ? ' checked ': '' ; ?> value="glutenfree"> glutenfree
                </label>
            </p>
            <p>
                <label for="other_namely_4">Other, namely</label>
                <input id="other_namely_4" class="input-xxlarge" name="other_namely_4" value="<?php echo $get_register_item->other_namely_4 ; ?>" type="text"/>
            </p>

            <p>
                <label for="food_allergies_4">Food allergies ?</label>
                <input id="food_allergies_4" class="input-xxlarge" name="food_allergies_4" value="<?php echo $get_register_item->food_allergies_4 ; ?>" type="text"/>
            </p>

            <p>
                <label for="special_assistance_4">Special assistance?</label>
                <input id="special_assistance_4" class="input-xxlarge" name="special_assistance_4" value="<?php echo $get_register_item->special_assistance_4 ; ?>" type="text"/>
            </p>

            <p>
                <label for="medical_condition_4">Medical condition?</label>
                <input id="medical_condition_4" class="input-xxlarge" name="medical_condition_4" value="<?php echo $get_register_item->medical_condition_4 ; ?>" type="text"/>
            </p>

        </div>

<div class="control-group">

    <legend>Logistics</legend>

    <p>
        <label for="live_outside_netherlands_4">Do you live outside The Netherlands?</label>
        <select id="live_outside_netherlands_4" name="live_outside_netherlands_4">
            <option value="0" <?php echo $get_register_item->live_outside_netherlands_4 == 0 ? " selected ": '' ; ?>>No</option>
            <option value="1" <?php echo $get_register_item->live_outside_netherlands_4 == 1 ? " selected ": '' ; ?>>Yes</option>
        </select>
    </p>

    <p>
        <label for="need_visa_to_visit_eu">Need a visa to visit the EU?</label>
        <select id="need_visa_to_visit_eu" name="need_visa_to_visit_eu">
            <option value="0" <?php echo $get_register_item->need_visa_to_visit_eu == 0 ? " selected ": '' ; ?>>No</option>
            <option value="1" <?php echo $get_register_item->need_visa_to_visit_eu == 1 ? " selected ": '' ; ?>>Yes</option>
        </select>
    </p>

    <p>
        <label for="inputPassN">Passport Number</label>
        <input id="inputPassN" class="input-xxlarge" name="inputPassN" value="<?php echo $get_register_item->inputPassN ; ?>" type="text"/>
    </p>

    <p>
        <label for="inputIssueOn">Issued on</label>
        <input id="inputIssueOn" class="input-xxlarge" name="inputIssueOn" value="<?php echo $get_register_item->inputIssueOn ; ?>" type="text"/>
    </p>

    <p>
        <label for="inputIssuePl">Place of issue</label>
        <input id="inputIssuePl" class="input-xxlarge" name="inputIssuePl" value="<?php echo $get_register_item->inputIssuePl ; ?>" type="text"/>
    </p>

    <p>
        <label for="inputPassNExpires">Expires</label>
        <input id="inputPassNExpires" class="input-xxlarge" name="inputPassNExpires" value="<?php echo $get_register_item->inputPassNExpires ; ?>" type="text"/>
    </p>

    <p>
        <label for="inputPlaceOfBirth">Your place of birth</label>
        <input id="inputPlaceOfBirth" class="input-xxlarge" name="inputPlaceOfBirth" value="<?php echo $get_register_item->inputPlaceOfBirth ; ?>" type="text"/>
    </p>


</div>



<div class="control-group">

    <legend>Accommodation in Rotterdam</legend>

    <p>
        <label for="date_of_arrival">Date of arrival</label>
        <input id="date_of_arrival" class="input-xxlarge" name="date_of_arrival" value="<?php echo $get_register_item->date_of_arrival ; ?>" type="text"/>
    </p>

    <p>
        <label for="date_of_departure">Date of departure</label>
        <input id="date_of_departure" class="input-xxlarge" name="date_of_departure" value="<?php echo $get_register_item->date_of_departure ; ?>" type="text"/>
    </p>

</div>



<div class="control-group">

    <span class="help-block">Gender</span>

    <label class="radio">
        <input id="genderOptionMale" <?php echo $get_register_item->gender_option_4=='Male'? ' checked="checked" ' : '' ; ?> type="radio"  name="genderOption4" value="Male">
        Male
    </label>

    <label class="radio">
        <input id="genderOptionFemale" type="radio"  <?php echo $get_register_item->gender_option_4=='Female'? ' checked="checked" ' : '' ; ?>  name="genderOption4" value="Female">
        Female
    </label>

</div>

<div class="control-group">

    <legend>Payment</legend>

    <p>
        <label for="payment_status">Payment Status</label>
        <select id="payment_status" name="payment_status">
            <option value="initialized" <?php echo $get_register_item->payment_status == 'initialized' ? " selected ": '' ; ?>>initialized</option>
            <option value="completed" <?php echo $get_register_item->payment_status == 'completed' ? " selected ": '' ; ?>>completed</option>
            <option value="uncleared" <?php echo $get_register_item->payment_status == 'uncleared' ? " selected ": '' ; ?>>uncleared</option>
            <option value="declined" <?php echo $get_register_item->payment_status == 'declined' ? " selected ": '' ; ?>>declined</option>
            <option value="canceled" <?php echo $get_register_item->payment_status == 'canceled' ? " selected ": '' ; ?>>canceled</option>
            <option value="refunded" <?php echo $get_register_item->payment_status == 'refunded' ? " selected ": '' ; ?>>refunded</option>
            <option value="expired" <?php echo $get_register_item->payment_status == 'expired' ? " selected ": '' ; ?>>expired</option>
            <option value="Unfinished Payment process" <?php echo $get_register_item->payment_status == 'Unfinished Payment process' ? " selected ": '' ; ?>>Unfinished Payment process</option>
        </select>
    </p>


    <p>
        <label for="total">Total</label>
        <input id="total" class="input-small" name="total" value="<?php echo $get_register_item->total ; ?>" type="text"/> €
    </p>

    <p>
        <label for="payment_transaction_id">Payment Transaction ID</label>
        <input id="payment_transaction_id" class="input-xxlarge" name="payment_transaction_id" value="<?php echo $get_register_item->payment_transaction_id ; ?>" type="text"/>
    </p>

    <p>
        <label for="date_of_departure">Date Creation</label>
    <p class="text-info"><?php echo date("y-m-d H:i:s", $get_register_item->date); ?></p>
    </p>

</div>
<p id="info-show2"></p>

<p>
    <input type="button" class="btn btn-primary" name="update_payment" id="update_payment" value="Update Item" />
</p>

</form>



