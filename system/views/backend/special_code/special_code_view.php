<script type="text/javascript">
    jQuery(document).ready(function($) {

        var $create_button      =    $("#newCodeBtn") ;
        var $new_code_field     =    $("#newCode") ;
        $create_button.click(function(){

                    var data2 = {
                                    action: 'add_special_code',
                                    data_to_send: $new_code_field.val()
                                };

                    $create_button.attr("disabled", "disabled");

                    $.post('admin-ajax.php', data2, function(response)
                    {
                        //alert("success");
                    })
                    .done(function(response)
                    {
                        //alert("second success");
                    })
                    .fail(function(response)
                    {
                        //alert("error");
                    })
                    .always(function(response)
                    {
                        //alert("finished");
                        $create_button.removeAttr("disabled");


                        $("#infoCode").html(response);

                    });
        });
    });
</script>
<div class="container-fluid">
    <div id="icon-themes" class="icon32"></div>
    <h2><?=$page_title;?></h2>
    <table id="specialCodeTable" class="table table-hover">
        <thead>
        <tr>
            <th>#CodeID</th>
            <th>Code</th>
            <th>Used</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($code_list as $code): ?>

            <tr class="success" id="tr-<?php  echo $code->id; ?>">
                <td><p class="text-info"><i class="icon-th-list"></i> <?php  echo $code->id; ?></p></td>
                <td><p class="text-info"><?php  echo $code->special_code; ?></p></td>
                <td><p class="text-info"><?php echo $check->checkSpecialCode($code->special_code)? '   ' : ' <i class="icon-ok-sign"></i> ' ; ?></p></td>
            </tr>

        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="form-inline">
        <p class="text-info" id="infoCode"></p>
        <input class="span2" type="text" id="newCode" name="newCode">
        <input type="button" class="btn" id="newCodeBtn" name="newCodeBtn" value="New Code">
    </div>
</div>