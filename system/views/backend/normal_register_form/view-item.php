<form id="update-item-view-3" name="update-item-view-3" method="post">
    <p id="info-show-3"></p>
    <div id="form-container">
        <input type="hidden" name="item_id_3" value="<?php echo isset($_GET['view_normal']) ? $_GET['view_normal'] : ''; ?>">


        <div class="control-group">
            <legend>Team information</legend>

            <p>
                <label for="team_name">Team name</label>
                <input id="team_name" class="input-xxlarge" name="team_name" value="<?php echo $get_register_item->team_name; ?>" type="text"/>
            </p>
            
            <legend>Experience of the main applicant:</legend>
            
            <p>
                <label for="experience">Could you give us a short summary or your previous experiences with having a business/setting up a start-up (use max. 300 words)?</label>
                <textarea id="experience" class="input-xxlarge" name="experience" ><?php echo $get_register_item->experience; ?></textarea>
                 </p>
            
            <legend>Motivation:</legend>

            <p>
                <label for="idea_description">Describe in short (not more than 250 words) your idea for this programme:</label>
                <input id="idea_description" class="input-xxlarge" name="idea_description" value="<?php echo $get_register_item->idea_description; ?>" type="text"/>
            </p>

            <p>
                <label for="how_far">How far are you with implementing your idea (use max. 100 words)?</label>
                <input id="how_far" class="input-xxlarge" name="how_far" value="<?php echo $get_register_item->how_far; ?>" type="text"/>
            </p>

            <p>
                <label for="problem_to_solve">What problem you are attempting to solve with your idea?</label>
                <input id="problem_to_solve" class="input-xxlarge" name="problem_to_solve" value="<?php echo $get_register_item->problem_to_solve; ?>" type="text"/>
            </p>

            <p>
                <label for="how_idea_solve">How will your idea solve this problem?</label>
                <input id="how_idea_solve" class="input-xxlarge" name="how_idea_solve" value="<?php echo $get_register_item->how_idea_solve; ?>" type="text"/>
            </p>

            <p>
                <label for="target_customer">Who is/are your target customer(s)?</label>
                <input id="target_customer" class="input-xxlarge" name="target_customer" value="<?php echo $get_register_item->target_customer; ?>" type="text"/>
            </p>

            <p>
                <label for="experience">Could you give us a short summary or your previous experiences with having a business/setting up a start-up (use max. 300 words)?</label>
                <input id="experience" class="input-xxlarge" name="experience" value="<?php echo $get_register_item->experience; ?>" type="text"/>
            </p>

            <p>
                <label for="motivation">What is your motivation to join this training programme? </label>
                <input id="motivation" class="input-xxlarge" name="motivation" value="<?php echo $get_register_item->motivation; ?>" type="text"/>
            </p>

            <p>
                <label for="hope_to_gain">What do you hope to gain from this training?</label>
                <input id="hope_to_gain" class="input-xxlarge" name="hope_to_gain" value="<?php echo $get_register_item->hope_to_gain; ?>" type="text"/>
            </p>

        </div>



        <div class="control-group">

            <legend>Have you ever attended or participated in acceleration programmes or entrepreneurship related trainings?</legend>
            <p>
                <label class="radio">
                    <input id="ticketOpt1" type="radio" <?php if ($get_register_item->other_participations == 1) {
    echo 'checked="checked"';
} ?> name="other_participations" value="1">
                    Yes
                </label>


                <label class="radio">
                    <input id="ticketOpt1" type="radio" <?php if ($get_register_item->other_participations == 0) {
    echo 'checked="checked"';
} ?> name="other_participations" value="0">
                    No
                </label>
            </p>

        </div>



        <p>
            <label for="wich_participations">If so which one(s):</label>
            <input id="wich_participations" class="input-xxlarge" name="wich_participations" value="<?php echo $get_register_item->wich_participations; ?>" type="text"/>
        </p>


        <div class="control-group">

            <legend>Have you ever run a start-up or business?</legend>
            <p>
                <label class="radio">
                    <input id="ticketOpt2" type="radio" <?php if ($get_register_item->run_start_up == 1) {
    echo 'checked="checked"';
} ?> name="run_start_up" value="1">
                    Yes
                </label>


                <label class="radio">
                    <input id="ticketOpt2" type="radio" <?php if ($get_register_item->run_start_up == 0) {
    echo 'checked="checked"';
} ?> name="run_start_up" value="0">
                    No
                </label>
            </p>

        </div>
        
        
        <p>
            <label for="how_many_start_up">If so how many?</label>
            <input id="how_many_start_up" class="input-xxlarge" name="how_many_start_up" value="<?php echo $get_register_item->how_many_start_up; ?>" type="text"/>
        </p>
        
        
         <div class="control-group">

            <legend>Are you currently running a start-up/business?</legend>
            <p>
                <label class="radio">
                    <input id="ticketOpt3" type="radio" <?php if ($get_register_item->current_start_up == 1) {
    echo 'checked="checked"';
} ?> name="current_start_up" value="1">
                    Yes
                </label>


                <label class="radio">
                    <input id="ticketOpt3" type="radio" <?php if ($get_register_item->current_start_up == 0) {
    echo 'checked="checked"';
} ?> name="current_start_up" value="0">
                    No
                </label>
            </p>

        </div>
        
        <p>
            <label for="name_your_business">What is the name of your business (if applicable)?</label>
            <input id="name_your_business" class="input-xxlarge" name="name_your_business" value="<?php echo $get_register_item->name_your_business; ?>" type="text"/>
        </p>
        
        
        <div class="control-group">

            <legend>Are you the founder/cofounder of that business (if applicable)?</legend>
            <p>
                <label class="radio">
                    <input id="ticketOpt4" type="radio" <?php if ($get_register_item->founder_cofounder == 0) {
    echo 'checked="checked"';
} ?> name="founder_cofounder" value="0">
                    Cofounder
                </label>


                <label class="radio">
                    <input id="ticketOpt4" type="radio" <?php if ($get_register_item->founder_cofounder == 1) {
    echo 'checked="checked"';
} ?> name="founder_cofounder" value="1">
                    Founder
                </label>
            </p>

        </div>
        

    </div>
    <p id="info-show2-3"></p>
    <p>
        <input type="button" class="btn btn-primary" name="update_payment_3" id="update_payment_3" value="Update Item" />
    </p>

</form>





