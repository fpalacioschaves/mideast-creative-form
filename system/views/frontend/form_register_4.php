
<form class="form-horizontal" id="formRegister" name="formRegister" method="get">

<input type="hidden" name="formRegisterOptionChoosed4" value="<?echo $choosed_option ; ?>">
<?php


$total              =  0;
if($tickChoosedOpt == '75')
{
    $total = "90.75";
}
elseif($tickChoosedOpt == '200')
{
    $total = "242";
}
elseif($tickChoosedOpt == '300')
{
    $total = "363";
}
elseif($tickChoosedOpt == '0')
{
    $total              =  0;
}


?>
<input type="hidden" name="formRegister4">
<input type="hidden" name="ticketOpt" id="ticketOpt" value="<?php echo $total; ?>">

<ul class="nav nav-tabs" id="myTabPages">
    <li><a href="#personalInformation" data-toggle="tab">Personal Information</a></li>
    <li><a href="#dietaryWishes" data-toggle="tab">Dietary Wishes</a></li>
    <li><a href="#Logistics" data-toggle="tab">Logistics</a></li>
    <li><a href="#accommodation" data-toggle="tab">Accommodation</a></li>
    <li><a href="#payment" data-toggle="tab">Payment</a></li>
</ul>
<div class="tab-content">
    <!---PERSONAL INFORMATION BLOCK--->
    <div class="tab-pane active" id="personalInformation">

        <legend>Personal information</legend>

        <p>
            <label for="inputYourName4">First name</label>
            <input id="inputYourName4" class="input-xxlarge" name="inputYourName4" type="text"/>
        </p>

        <p>
            <label for="inputYourFamName4">Family name</label>
            <input id="inputYourFamName4"  class="input-xxlarge" type="text" name="inputYourFamName4"  />
        </p>
        <div class="control-group">

            <label for="inputYourName4">Gender</label>

            <label class="radio">
                <input id="genderOptionMale4" type="radio" checked="checked" name="genderOption4" value="Male">
                Male
            </label>

            <label class="radio">
                <input id="genderOptionFemale4" type="radio"  name="genderOption4" value="Female">
                Female
            </label>

        </div>



        <p>
            <label for="inputYourEmail4">Email</label>
            <input id="inputYourEmail4" class="input-xxlarge" name="inputYourEmail4" type="email"/>
        </p>

        <p>
            <label for="inputMobilePhone4">Mobile Phone</label>
            <input id="inputMobilePhone4" class="input-xxlarge" placeholder="31 111111111" name="inputMobilePhone4" type="tel"/>
        </p>

        <p>
            <label for="inputStreet4">Street</label>
            <input id="inputStreet4" class="input-xxlarge" name="inputStreet4" type="text"/>
        </p>

        <p>
            <label for="inputZipCode4">Zip code</label>
            <input id="inputZipCode4" class="input-xxlarge" name="inputZipCode4" type="text"/>
        </p>

        <p>
            <label for="inputCity4">City</label>
            <input id="inputCity4" class="input-xxlarge" name="inputCity4" type="text"/>
        </p>

        <p>
            <label for="inputCountry4">Country</label>
            <input id="inputCountry4" class="input-xxlarge" name="inputCountry4" type="text"/>
        </p>

        <p>
            <label for="inputNationality4">Nationality</label>
            <input id="inputNationality4" class="input-xxlarge" name="inputNationality4" type="text"/>
        </p>




        <legend >Are you affiliated with an organization</legend>

        <div class="control-group">

            <label class="radio">
                <input id="affiliatedOpt1" type="radio" radio-tag="Are you affiliated with an organization" checked="checked" name="affiliatedAsk" value="1">
                Yes
            </label>

            <label class="radio">
                <input id="affiliatedOpt2" type="radio" radio-tag="Are you affiliated with an organization" checked="checked" name="affiliatedAsk" value="0">
                No
            </label>

        </div>

        <div id="affiliatedView"></div>

        <legend>Contact in case of emergency:</legend>

        <p>
            <label for="inputEmergencyName4">Name</label>
            <input id="inputEmergencyName4" class="input-xxlarge" name="inputEmergencyName4" type="text"/>
        </p>

        <p>
            <label for="inputEmergencyRelationshipAff4">Relationship / Affiliation</label>
            <input id="inputEmergencyRelationshipAff4" class="input-xxlarge" name="inputEmergencyRelationshipAff4" type="text"/>
        </p>

        <p>
            <label for="inputEmergencyMobilePhone4">Phone number:</label>
            <input id="inputEmergencyMobilePhone4" class="input-xxlarge"  name="inputEmergencyMobilePhone4" placeholder="31 111111111"  type="tel"/>
        </p>






        <div class="control-group">

            <legend>Where did you hear from this event?</legend>

            <label class="checkbox" for="hearFromThisEvent4-1">
                <input type="checkbox" id="hearFromThisEvent4-1" name="hearFromThisEvent4[1]" value="I was invited by Hivos">  I was invited by Hivos
            </label>

            <label class="checkbox" for="hearFromThisEvent4-2">
                <input type="checkbox" id="hearFromThisEvent4-2" name="hearFromThisEvent4[2]" value="via a colleague or contact of mine">  via a colleague or contact of mine
            </label>

            <label class="checkbox" for="hearFromThisEvent4-3">
                <input type="checkbox" id="hearFromThisEvent4-3" name="hearFromThisEvent4[3]" value="via the Hivos website">
                 via the Hivos website
            </label>

            <div class="control-group" >
                <label class="checkbox" for="inputHearFromThisEventOther4Ask">
                    <input id="inputHearFromThisEventOther4Ask" type="checkbox" name="inputHearFromThisEventOther4Ask" value="1">Other
                </label>

                <div id="inputHearFromThisEventOther4View"></div>
            </div>
        </div>

        <p>
            <input type="button" class="btn-large btn btn-inverse" id="nextTab1" name="nextTab1" value="Next Tab">
            <span class="help-block" id="infoFill"></span>
        </p>

    </div>
    <!---END OF PERSONAL INFORMATION BLOCK--->

    <!---DIETARY WISHES AND MEDICAL CONDITIONS BLOCK--->
    <div class="tab-pane" id="dietaryWishes">

        <legend>Dietary wishes and medical conditions</legend>

        <div class="control-group">

            <span class="help-block">Do you have any dietary requirements?</span>

            <label class="radio">
                <input id="dietaryRequirements1" type="radio" checked="checked" name="dietaryRequirementsAsk" value="1">
                Yes
            </label>

            <label class="radio">
                <input id="dietaryRequirements2" type="radio" checked="checked" name="dietaryRequirementsAsk" value="0">
                No
            </label>

        </div>

        <div id="dietaryRequirements"></div>

        <p>
            <input type="button" class="btn-large btn btn-inverse" id="nextTab2" name="nextTab2" value="Next Tab">
        </p>


    </div>
    <!---FIN OF DIETARY WISHES AND MEDICAL CONDITIONS BLOCK--->


    <!---LOGISTICS BLOCK--->
    <div class="tab-pane" id="Logistics">

        <legend>Logistics</legend>

        <div class="control-group">

            <span class="help-block">Do you live outside The Netherlands?</span>

            <p>
                <label class="radio">
                    <input id="outsideTheNetherland4-1" type="radio" checked="checked" name="outsideTheNetherland4" value="1">
                    Yes
                </label>

                <label class="radio">
                    <input id="outsideTheNetherland4-2" type="radio"  name="outsideTheNetherland4" value="0">
                    No
                </label>
            </p>

        </div>

        <div id="trn"></div>

        <p>
            <input type="button" class="btn-large btn btn-inverse" id="nextTab3" name="nextTab3" value="Next Tab">
        </p>

    </div>
    <!---FIN LOGISTICS BLOCK--->

<!---ACCOMODATION BLOCK--->
<div class="tab-pane" id="accommodation">

    <legend>Accommodation in Rotterdam</legend>

    <div class="control-group">

        <p>
            ‘Open for Change’ has reserved rooms at Savoy Hampshire Hotel, Hoogstraat 81, Rotterdam.
            We have agreed on a price of 80 euros per night, incl. breakfast.
            A free shuttle bus will operate between this hotel and the event venue.
            Hivos is able to cover accommodation costs of a limited number of international participants.
            We have communicated this with them in personal correspondence."
        </p>

        <span class="help-block">Do you need accommodation?</span>

        <label class="radio">
            <input id="accommodationOpt1" type="radio" checked="checked" name="accommodationAsk" value="1"> Yes
        </label>

        <label class="radio">
            <input id="accommodationOpt2" type="radio" checked="checked" name="accommodationAsk" value="0"> No
        </label>

    </div>

    <p id="accommodationView"></p>
    <p>
        <input type="button" class="btn-large btn btn-inverse" id="nextTab4" name="nextTab4" value="Next Tab">
    </p>


</div>

<!---FIN ACCOMODATION BLOCK--->
<div class="tab-pane" id="payment">

    <legend>Your Details</legend>

    <p id="fullFormInfo"></p>


    <p id="personalInfoRes">


    <table class="table table-striped" id="personalInformationTable">
        <thead>
        <tr>
            <th>Personal Information</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>First name</td>
            <td id="first-name-info"></td>
        </tr>
        <tr>
            <td>Family name</td>
            <td id="family-name-info"></td>
        </tr>
        <tr>
            <td>Email</td>
            <td id="email-info"></td>
        </tr>
        <tr>
            <td>Mobile Phone</td>
            <td id="mobile-phone-info"></td>
        </tr>

        <tr>
            <td>Street</td>
            <td id="street-info"></td>
        </tr>
        <tr>
            <td>Zip code</td>
            <td id="zip-code-info"></td>
        </tr>
        <tr>
            <td>City</td>
            <td id="city-info"></td>
        </tr>
        <tr>
            <td>Country</td>
            <td id="country-info"></td>
        </tr>
        <tr>
            <td>Nationality</td>
            <td id="nationality-info"></td>
        </tr>
        </tbody>
    </table>
    </p>



    <p id="affiliatedOrganizationResult"></p>

    <p id="ContactEmergencyRes">


    <table class="table table-striped" id="ContactEmergencyTable">
        <thead>
        <tr>
            <th>Contact in case of emergency</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Name</td>
            <td id="relationship-name-info"></td>
        </tr>
        <tr>
            <td>Relationship / Affiliation</td>
            <td id="relationship-affiliation-info"></td>
        </tr>
        <tr>
            <td>Phone number:</td>
            <td id="relationship-number-info"></td>
        </tr>
        </tbody>
    </table>
    </p>

    <p id="hearThisEvRes">
    <table class="table table-striped" id="hearThisEvTable">
        <thead>
        <tr>
            <th>Where did you hear from this event?</th>
            <th></th>
        </tr>
        </thead>
        <tbody>


        </tbody>
    </table>
    </p>
    <p id="inputFoodAllergiesRes"></p>
    <p id="medicalConditionsRes"></p>
    <p id="inputIdAssistanceRes"></p>
    <p id="inputIdMedicalRes"></p>
    <p id="outsideTheNetherland4Res"></p>
    <p id="visitEUVisa4Res"></p>
    <p id="accommodationOpt1Res"></p>
    <p id="hivosCoverYourAcommodationRes"></p>

    <table class="table table-condensed">

        <thead>
        <tr>
            <th>Payment</th>
            <th></th>
        </tr>
        </thead>

        <tbody>


        <tr>
                <td>Ticket : (Regular)</td>
                <td>
                    <span  class="pull-right">
                <?php
                    if($tickChoosedOpt=='75')
                    {
                        echo "€90.75 " ;
                    }
                    elseif($tickChoosedOpt=='200')
                    {
                        echo "€242 (Large NGO)" ;
                    }
                    elseif($tickChoosedOpt=="300")
                    {
                        echo "€363 (Medium or largen business)" ;
                    }
                    elseif($tickChoosedOpt=="0")
                    {
                        echo "(Free of charge)" ;
                    }
                    ?><span id="optCh"></span>
                    </span>
              <!--  <p>
                    <span class="help-block">Your ticket covers the event, food and drinks and an event welcome package.</span>
                </p>-->

            </td>
        </tr>

<tr>
    <td><span class="help-block">Your ticket covers the event, food and drinks and an event welcome package.</span></td><td></td>
</tr>




        <tr id="tickVal">
            <?php if($supportOpt): ?>
            <td>Support ticket: </td>
            <td><span class="pull-right">€90.75</span></td>
            <?php endif; ?>
        </tr>


        <tr>
            <td>Total:</td>
            <td id="totalAmount"><span class="pull-right">€<?php echo $total; ?></span></td>
        </tr>

        <tr><td></td><td><span class="pull-right">incl vat</span></td></tr>






        <tr>
            <td></td>
            <td></td>
        </tr>


        </tbody>
    </table>



    <label class="checkbox">
        <input id="supportOption" type="checkbox"  name="supportOption" value="1" <?php echo $supportOpt? ' checked ': '' ;?>>
        I would like to support a (foreign) participant in less fortunate financial circumstances -> € 75 ex. VAT (€ 90,75 incl. VAT)
    </label>

    <p>
        <input type="button" class="btn-large btn btn-inverse" id="nextStep" name="nextStep" value="Checkout">
        <span class="help-block"><small><em>To pay for your ticket you will be redirected to our payment gateway after you click checkout. We work with MultiSafePay, who will guide you safely through your online payment. You fill in your details, choose your payment method and a connection is made with the payment system MultiSafePay. You can pay via Ideal, Visa or MasterCard, Maestro and Mister Cash.</em></small></span>
    </p>



</div>

</div>

</form>
<p><small> Your personal information will be treated with care and will not be used or disclosed for purposes other than the facilitation of your stay at the event. We will not distribute this content among any third parties other than those assisting Hivos in the organization of this event.</small></p>
<script type="text/javascript">
jQuery(document).ready(function ($)
{

    var data = {action: 'view_transport_to_nl'};

    $.post('/wp-admin/admin-ajax.php', data, function (response) {
        $("#trn").html(response);
    });


    $('[id^=inputMobilePhone4]').keypress(validateNumber);
    $('[id^=inputEmergencyMobilePhone4]').keypress(validateNumber);



    if ($("#supportOption").is(':checked'))
    {

        var fn = <?php echo $total ;?>;
        var ln = 90.75 ;
        var fp = (fn+ln) ;
        $("#totalAmount").html('<span class="pull-right">€'+ fp + '</span>');
    }


    $("#supportOption").click(function () {

        var fn = <?php echo $total ;?>;
        var ln = 90.75 ;
        if ($(this).is(':checked'))
        {
            var fp = (fn+ln) ;
            $("#totalAmount").html('<span class="pull-right">€'+fp + '</span>');
            $("#tickVal").html("<td>Support ticket: </td><td><span class='pull-right'>€90.75</span></td>");
        }
        else
        {
            $("#totalAmount").html('<span class="pull-right">€' + fn + '</span>');
            $("#tickVal").html("");
        }
    });


    $('a[href="#payment"]').on('shown', function (e)
    {

        $("#first-name-info").html($("#inputYourName4").val());
        $("#family-name-info").html($("#inputYourFamName4").val());
        $("#email-info").html($("#inputYourEmail4").val());
        $("#mobile-phone-info").html($("#inputMobilePhone4").val());
        $("#street-info").html($("#inputStreet4").val());
        $("#zip-code-info").html($("#inputZipCode4").val());
        $("#city-info").html($("#inputCity4").val());
        $("#country-info").html($("#inputCountry4").val());
        $("#nationality-info").html($("#inputNationality4").val());

        if ($("#affiliatedOpt1").is(':checked'))
        {
            $("#affiliatedOrganizationResult").html(
                '<table class="table table-striped" id="affiliatedOrganization">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Affiliated with ?</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +

                    '<tr>' +
                    '<td>Organization</td>' +
                    '<td id="organization-info"></td>' +
                    '</tr>'+

                    '<tr>' +
                    '<td>Organization Street</td>' +
                    '<td id="organization-street-info"></td>' +
                    '</tr>'+

                    '<tr>' +
                    '<td>Organization Zip code</td>' +
                    '<td id="organization-zip-code-info"></td>' +
                    '</tr>'+

                    '<tr>' +
                    '<td>Organization City</td>' +
                    '<td id="organization-city-info"></td>' +
                    '</tr>'+

                    '<tr>' +
                    '<td>Organization Country</td>' +
                    '<td id="organization-country-info"></td>' +
                    '</tr>'+

                    '<tr>' +
                    '<td>Phone number of organization</td>' +
                    '<td id="organization-phone-info"></td>' +
                    '</tr>'+

                    '</tbody>' +
                    '</table>'
            );
            $("#organization-info").html($("#inputAffiliatedOrganization4").val());
            $("#organization-street-info").html($("#inputOrgStreet4").val());
            $("#organization-zip-code-info").html($("#inputOrgZipCode4").val());
            $("#organization-city-info").html($("#inputOrgCity4").val());
            $("#organization-country-info").html($("#inputOrgCountry4").val());
            $("#organization-phone-info").html($("#inputOrgMobilePhone4").val());
        }
        else
        {
            $("#affiliatedOrganizationResult").html("");
        }

        $("#relationship-name-info").html($("#inputEmergencyName4").val());
        $("#relationship-affiliation-info").html($("#inputEmergencyRelationshipAff4").val());
        $("#relationship-number-info").html($("#inputEmergencyMobilePhone4").val());


        $("#hearThisEvTable > tbody").html("");
        if($("#hearFromThisEvent4-1").is(':checked'))
        {
            $("#hearThisEvTable > tbody").append("<tr><td>I was invited by Hivos</td></tr>");
        }


        if($("#hearFromThisEvent4-2").is(':checked'))
        {
            $("#hearThisEvTable > tbody").append("<tr><td>Via a colleague or contact of mine</td></tr>");
        }

        if($("#hearFromThisEvent4-3").is(':checked'))
        {
            $("#hearThisEvTable > tbody").append("<tr><td>via the Hivos website</td></tr>");
        }


        if($("#inputHearFromThisEventOther4Ask").is(':checked'))
        {
            $("#hearThisEvTable > tbody").append("<tr><td>"+ $("#inputHearFromThisEventOther4").val()+"</td></tr>");
        }


        if ($("#dietaryRequirements1").is(':checked'))
        {
            $("#dietaryRequirementsTable > tbody").html("");
            $("#medicalConditionsRes").html(
                '<table class="table table-striped" id="dietaryRequirementsTable">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Do you have any dietary requirements?</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                    '</table>'
            );

            if($("#inlineCheckbox3-1").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Vegetarian</td></tr>");
            }
            if($("#inlineCheckbox3-2").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Pescatarian</td></tr>");
            }
            if($("#inlineCheckbox3-3").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Vegan</td></tr>");
            }
            if($("#inlineCheckbox3-4").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Helal</td></tr>");
            }
            if($("#inlineCheckbox3-5").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Kosjer</td></tr>");
            }
            if($("#inlineCheckbox3-6").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>Glutenfree</td></tr>");
            }
            if($("#inputOtherNamely3Ask").is(':checked'))
            {
                $("#dietaryRequirementsTable > tbody").append("<tr><td>"+$("#inputOtherNamely3").val()+"</td></tr>");
            }




            if ($("#inputFoodAllergies3-1").is(':checked'))
            {
                $("#inputFoodAllergiesTable > tbody").html("");
                $("#inputFoodAllergiesRes").html(
                    '<table class="table table-striped" id="inputFoodAllergiesTable">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Do you have any (food) allergies we should know of?</th>' +
                        '<th></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                );

                $("#inputFoodAllergiesTable > tbody").append("<tr><td>"+$("#inputFoodAllergies3").val()+"</td></tr>");
            }
            else
            {
                $("#inputFoodAllergiesRes").html("");
            }




            if ($("#inputIdAssistance3-1").is(':checked'))
            {
                $("#inputIdAssistanceTable > tbody").html("");
                $("#inputIdAssistanceRes").html(
                    '<table class="table table-striped" id="inputIdAssistanceTable">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Do you require special assistance?</th>' +
                        '<th></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                );

                $("#inputIdAssistanceTable > tbody").append("<tr><td>"+$("#inputIdAssistance3").val()+"</td></tr>");
            }
            else
            {
                $("#inputIdAssistanceRes").html("");
            }


            if ($("#inputIdMedical3-1").is(':checked'))
            {
                $("#inputIdMedicalTable > tbody").html("");
                $("#inputIdMedicalRes").html(
                    '<table class="table table-striped" id="inputIdMedicalTable">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Do you suffer from a medical condition we should know of?</th>' +
                        '<th></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                );

                $("#inputIdMedicalTable > tbody").append("<tr><td>"+$("#inputIdMedical3").val()+"</td></tr>");
            }
            else
            {
                $("#inputIdMedicalRes").html("");
            }
        }
        else
        {
            $("#medicalConditionsRes").html("");
        }


        if ($("#outsideTheNetherland4-1").is(':checked'))
        {
            $("#outsideTheNetherland4Table > tbody").html("");
            $("#outsideTheNetherland4Res").html(
                '<table class="table table-striped" id="outsideTheNetherland4Table">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Logistics: Do you live outside The Netherlands?</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                    '</table>'
            );

            $("#outsideTheNetherland4Table > tbody").append("<tr><td>Yes</td></tr>");

            if ($("#visitEUVisa4-1").is(':checked'))
            {
                $("#visitEUVisa4Table > tbody").html("");
                $("#visitEUVisa4Res").html(
                    '<table class="table table-striped" id="visitEUVisa4Table">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Transport to the Netherlands: I need a visa to visit the European Union</th>' +
                        '<th></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                );

                $("#visitEUVisa4Table > tbody").append("<tr><td>Yes</td></tr>");
            }
            else
            {
                $("#visitEUVisa4Res").html("");
            }
        }
        else
        {
            $("#outsideTheNetherland4Res").html("");
        }




        if ($("#accommodationOpt1").is(':checked'))
        {
            $("#accommodationOpt1Table > tbody").html("");
            $("#accommodationOpt1Res").html(
                '<table class="table table-striped" id="accommodationOpt1Table">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Do you need accommodation?</th>' +
                    '<th></th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>' +
                    '</tbody>' +
                    '</table>'
            );

            $("#accommodationOpt1Table > tbody").append("<tr><td>Yes</td><td></td></tr>");

            $("#accommodationOpt1Table > tbody").append("<tr><td>Date of arrival</td><td>"+$("#inputDateOfArrival4").val()+"</td></tr>");
            $("#accommodationOpt1Table > tbody").append("<tr><td>Date of departure</td><td>"+$("#inputDateOfDeparture4").val()+"</td></tr>");


            if ($("#hivosCoverYourAcommodation-1").is(':checked'))
            {
                $("#hivosCoverYourAcommodationTable > tbody").html("");
                $("#hivosCoverYourAcommodationRes").html(
                    '<table class="table table-striped" id="hivosCoverYourAcommodationTable">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Does Hivos cover your acommodation?</th>' +
                        '<th></th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                        '</table>'
                );

                $("#hivosCoverYourAcommodationTable > tbody").append("<tr><td>Yes</td></tr>");
            }
            else
            {
                $("#hivosCoverYourAcommodationRes").html("");
            }

        }
        else
        {
            $("#accommodationOpt1ResvisitEUVisa4Res").html("");
        }

    });


    function IsJsonString(str)
    {
        try
        {
            JSON.parse(str);
        }
        catch (e)
        {
            return false;
        }
        return true;
    }

    var $create_button  = $("#nextStep");
    var $form           = $("#formRegister");

    function ajax_req() {
        var data = {
            action: 'register_process',
            data_to_send: $form.serialize()
        };

        $create_button.attr("disabled", "disabled");

        $.post('/wp-admin/admin-ajax.php', data, function (response) {

            $create_button.removeAttr("disabled");

            if (IsJsonString(response)) {

                var data = jQuery.parseJSON(response);

                if (data.errorElements)
                {

                    $("#terr").remove();
                    for (var el in data.errorElements)
                    {
                        var label = $('label[for=' + el + ']', $form),
                            formElement = $('#' + el, $form);

                        formElement.addClass('text-error');

                        if (data.errorElements[el] != "" && label.length)
                        {
                            label.append('<span class="text-error" id="terr">' + data.errorElements[el] + '</span>');
                        }
                    }

                    if (data.scrollTo)
                    {
                        $('body').scrollTo(data.scrollTo);
                    }
                }
                else if (data.success)
                {
                    $("#form-container").html(data.success);
                }
            }
            else
            {
                $("#form-container").html(response);
            }
        });

    }

    $create_button.click(function () {
        ajax_req();
    });

    $("#inputHearFromThisEventOther4Ask").change(function () {
        if ($("#inputHearFromThisEventOther4Ask").is(':checked'))
        {
            $("#inputHearFromThisEventOther4View").html('<input id="inputHearFromThisEventOther4" class="input-xxlarge" name="inputHearFromThisEventOther4" type="text" />');
        }
        else
        {
            $("#inputHearFromThisEventOther4View").html('');
        }
    });

    $("#nextTab1").click(function () {

        $('#myTabPages a[href="#dietaryWishes"]').tab('show');

        checkPersonalInfo();

    });

    $("#nextTab2").click(function () {

        $('#myTabPages a[href="#Logistics"]').tab('show');
        checkPersonalInfo();

    });

    $("#nextTab3").click(function () {

        $('#myTabPages a[href="#accommodation"]').tab('show');
        checkPersonalInfo();

    });

    $("#nextTab4").click(function () {

        $('#myTabPages a[href="#payment"]').tab('show');
        accommodation
        checkPersonalInfo();

    });


    $('a[data-toggle="tab"]').on('shown', function (e) {window.location.hash =  e.target.hash; });

    $(window).hashchange(function(e, data) {
        var hash = data.after ;
        $('#myTabPages a[href="'+hash+'"]').tab('show');
    });


    var dietaryRequirements = $("#dietaryRequirements");

    $("[name='dietaryRequirementsAsk']").click(function () {

        if ($(this).val() == '1')
        {

            var data = {action: 'load_dietary_wishes'};

            $.post('/wp-admin/admin-ajax.php', data, function (response) {
                dietaryRequirements.html(response);
            });
        }
        else
        {
            dietaryRequirements.html("");
        }
    });



    var affiliatedView = $("#affiliatedView");
    $("[name='affiliatedAsk']").click(function () {

        if ($(this).val() == '1')
        {
            var data = {action: 'load_affiliated'};

            $.post('/wp-admin/admin-ajax.php', data, function (response) {
                affiliatedView.html(response);
            });
        }
        else
        {
            affiliatedView.html("");
        }
    });

    var accommodationView = $("#accommodationView");
    $("[name='accommodationAsk']").click(function () {
        if ($(this).val() == '1')
        {
            var data = {action: 'load_accommodation'};
            $.post('/wp-admin/admin-ajax.php', data, function (response) {
                accommodationView.html(response);
            });
        }
        else
        {
            accommodationView.html("");
            $("#checkIn").html("") ;
            $("#checkOut").html("") ;
        }
    });






    $("[name='outsideTheNetherland4']").click(function () {

        if ($(this).val() == '1')
        {
            var data = {action: 'view_transport_to_nl'};

            $.post('/wp-admin/admin-ajax.php', data, function (response) {
                $("#trn").html(response);
            });
        }
        else if ($(this).val() == '0')
        {
            $("#trn").html('');
        }
    });

    $('#myTabPages a:first').tab('show');

    $('a[data-toggle="tab"]').on('shown', function (e) {
        e.target // activated tab
        e.relatedTarget // previous tab
        checkPersonalInfo();
    })




    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;

        if (event.keyCode == 8 || event.keyCode == 46
            || event.keyCode == 37 || event.keyCode == 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    };
    function ValidateEmail(mail)
    {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
        {
            return (true)
        }
        return (false)
    }



    function checkPersonalInfo() {
           var $i = $("#personalInformation :input");


        if(ValidateEmail($("#inputYourEmail4").val()))
        {
             var check = new Array();
             $i.each(function (i) {
             //$('label[for=' + $(this).attr('id') + ']').removeClass('text-error') ;

             if ($(this).val() == '') {
             check.push(i);
             $('label[for=' + $(this).attr('id') + ']').addClass('text-error');
             }
             else {
             $('label[for=' + $(this).attr('id') + ']').removeClass('text-error');
             }
             });

             if (check.length > 0) {
             $("#infoFill").html("*Please fill in the fields marked in red") ;
             $('#myTabPages a[href="#personalInformation"]').tab('show');
             }
             else
             {
             $("#infoFill").html(" ") ;
             }
        }else
        {
            $('label[for=inputYourEmail4]').addClass('text-error');
            $("#infoFill").html("*Please fill in the fields marked in red") ;
            $('#myTabPages a[href="#personalInformation"]').tab('show');
        }

    }
});
</script>