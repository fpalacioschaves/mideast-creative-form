<div id="form-container">
<form class="form-horizontal" >
    <legend>Tickets and registration</legend>

    <p>
        <span class="help-block">Welcome!</span>
    </p>
      <p>Open for Change is a three-day event organized by Hivos. The event starts in the afternoon of Thursday October 10th, when we kick-off with an <a href="/ignite/">Ignite</a> which is open to everyone, and we warmly invite you to join us! Attendance is free of charge, there will be food and drinks.</p>

      <p>On Friday 11 and Saturday 12 October, we will exchange, explore and experiment around the theme of the Open Society. A ticket is required to attend this event. You can attend both parts of the event, but also chose to only attend Ignite or the two-day event. Please indicate what parts of Open for Change you wish to attend. Be mindful that you can only choose one of the options listed below.</p>

    <p>
        <a href="?request=3" class="a"> Ignite – October 10th, 18u-21u. Open to everybody, free of charge.</a>
    </p>

    <p>
            <a href="?request=plus_2_day_event" class="a"> Ignite and the two-day event  – October 10-12. A ticket is required.</a>
    </p>

    <p>
            <a href="?request=2_day_event" class="a"> Only the two-day event – October 11 &amp; 12. A ticket is required</a>
    </p>


</form>
</div>