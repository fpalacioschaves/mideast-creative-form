<div class="control-group">

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-1" name="inlineCheckbox3[1]" value="vegetarian"> vegetarian
        </label>

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-2" name="inlineCheckbox3[2]" value="pescatarian"> pescatarian
        </label>

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-3" name="inlineCheckbox3[3]" value="vegan"> vegan
        </label>

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-4" name="inlineCheckbox3[4]" value="helal"> helal
        </label>

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-5" name="inlineCheckbox3[5]" value="kosjer"> kosjer
        </label>

        <label class="checkbox ">
            <input type="checkbox" id="inlineCheckbox3-6" name="inlineCheckbox3[6]" value="glutenfree"> glutenfree
        </label>




    <div class="control-group">


        <label class="checkbox ">
            <input id="inputOtherNamely3Ask" type="checkbox" name="inputOtherNamely3Ask" value="1">Other, namely
        </label>

        <div id="inputOtherNamely3View"></div>
    </div>


    <div class="control-group">

        <div class="control">
            <span class="help-block">Do you have any (food) allergies we should know of?</span>
        </div>

        <label class="radio">
            <input id="inputFoodAllergies3-1" type="radio" checked="checked" name="inputFoodAllergies3Ask" value="1">
            Yes
        </label>
        <label class="radio">
            <input id="inputFoodAllergies3-2" type="radio" checked="checked" name="inputFoodAllergies3Ask" value="0">
            No
        </label>

        
        <div id="inputFoodAllergies3View"></div>

</div>


<div class="control-group">

    <div class="control">
        <span class="help-block">Do you require special assistance? </span>
    </div>

    <label class="radio">
        <input id="inputIdAssistance3-1" type="radio" checked="checked" name="inputIdAssistance3Ask" value="1">
        Yes
    </label>
    <label class="radio">
        <input id="inputIdAssistance3-2" type="radio" checked="checked" name="inputIdAssistance3Ask" value="0">
        No
    </label>

    
    <div id="inputIdAssistance3View"></div>

</div>

<div class="control-group">

    <div class="control">
        <span class="help-block">Do you suffer from a medical condition we should know of? </span>
    </div>

    <label class="radio">
        <input id="inputIdMedical3-1" type="radio" checked="checked" name="inputIdMedical3Ask" value="1">
        Yes
    </label>
    <label class="radio">
        <input id="inputIdMedical3-2" type="radio" checked="checked" name="inputIdMedical3Ask" value="0">
        No
    </label>

    
    <div id="inputIdMedical3View"></div>

</div>

</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $("[name='inputFoodAllergies3Ask']").click(function(){
            if($(this).val()=='1')
            {
                $("#inputFoodAllergies3View").html('<input id="inputFoodAllergies3" placeholder="Which food allergies do you have?" class="input-xxlarge" name="inputFoodAllergies3" type="text" />');
            }
            else
            {
                $("#inputFoodAllergies3View").html('');
            }
        });

        $("[name='inputIdAssistance3Ask']").click(function(){
            if($(this).val()=='1')
            {
                $("#inputIdAssistance3View").html('<input id="inputIdAssistance3" class="input-xxlarge"  placeholder="What special assistance do you need?" name="inputIdAssistance3" type="text" />');
            }
            else
            {
                $("#inputIdAssistance3View").html('');
            }
        });

        $("[name='inputIdMedical3Ask']").click(function(){
            if($(this).val()=='1')
            {
                $("#inputIdMedical3View").html('<input id="inputIdMedical3" class="input-xxlarge" placeholder="What medical condition you have?" name="inputIdMedical3" type="text" />');
            }
            else
            {
                $("#inputIdMedical3View").html('');
            }
        });

        $("#inputOtherNamely3Ask").change(function() {
            if($("#inputOtherNamely3Ask").is(':checked'))
            {
                $("#inputOtherNamely3View").html('<input id="inputOtherNamely3" class="input-xxlarge" name="inputOtherNamely3" type="text" />');
            }
            else
            {
                $("#inputOtherNamely3View").html('');
            }

        });


    });
</script>