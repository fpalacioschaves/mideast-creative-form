<p>
    <legend>Affiliated with ?</legend>
    <label for="inputAffiliatedOrganization4">Organization:</label>

    <input id="inputAffiliatedOrganization4" class="input-xxlarge" name="inputAffiliatedOrganization4" type="text"/>
    <span class="help-block">If affiliated with multiple organizations, you are free to indicate this.</span>
</p>

<p>
    <label for="inputOrgStreet4">Street</label>
    <input id="inputOrgStreet4" class="input-xxlarge" name="inputOrgStreet4" type="text"/>
</p>

<p>
    <label for="inputOrgZipCode4">Zip code</label>
    <input id="inputOrgZipCode4" class="input-xxlarge" name="inputOrgZipCode4" type="text"/>
</p>

<p>
    <label for="inputOrgCity4">City</label>
    <input id="inputOrgCity4" class="input-xxlarge" name="inputOrgCity4" type="text"/>
</p>

<p>
    <label for="inputOrgCountry4">Country</label>
    <input id="inputOrgCountry4" class="input-xxlarge" name="inputOrgCountry4" type="text"/>
</p>

<p>
    <label for="inputOrgMobilePhone4">Phone number of organization</label>
    <input id="inputOrgMobilePhone4" class="input-xxlarge" name="inputOrgMobilePhone4" placeholder="31 111111111" type="text"/>
</p>

<script type="text/javascript">
    jQuery(document).ready(function ($)
    {


$('[id^=inputOrgMobilePhone4]').keypress(validateNumber);
        function validateNumber(event) {
            var key = window.event ? event.keyCode : event.which;

            if (event.keyCode == 8 || event.keyCode == 46
                || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if ( key < 48 || key > 57 ) {
                return false;
            }
            else return true;
        };
        function ValidateEmail(mail)
        {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
            {
                return (true)
            }
            return (false)
        }

    });
</script>