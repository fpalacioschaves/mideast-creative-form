<div class="control-group">
    <div class="control">
    <span class="help-block">Our programme team will contact you within three days, to help you arrange your visa.</span>

        <br />
        <p>
            <label for="inputPassN">Please fill in your personal passport number:</label>
            <input id="inputPassN" class="input-xxlarge" name="inputPassN" required type="text"/>
        </p>


        <p>
            <label for="inputIssueOn">Issued on</label>
            <input id="inputIssueOn" class="input-xxlarge" name="inputIssueOn" required type="text"/>
        </p>


        <p>
            <label for="inputIssuePl">Place of issue:</label>
            <input id="inputIssuePl" class="input-xxlarge" name="inputIssuePl" required type="text"/>
        </p>


        <p>
            <label for="inputPassNExpires">Expires:</label>
            <input id="inputPassNExpires" class="input-xxlarge" name="inputPassNExpires" required type="text"/>
        </p>


        <p>
            <label for="inputPlaceOfBirth">Your place of birth:</label>
            <input id="inputPlaceOfBirth" class="input-xxlarge" name="inputPlaceOfBirth" required type="text"/>
        </p>

    </div>

</div>


<script type="text/javascript">
    jQuery(document).ready(function ($)
    {

        $('a[data-toggle="tab"]').on('shown', function (e) {
            e.target // activated tab
            e.relatedTarget // previous tab

            if($("#inputPassN").val()=='')
            {
                $('#myTabPages a[href="#Logistics"]').tab('show');
                $('label[for=inputPassN]').addClass('text-error');
            }
            else
            {
                $('label[for=inputPassN]').removeClass('text-error');
            }

            if($("#inputIssueOn").val()=='')
            {
                $('#myTabPages a[href="#Logistics"]').tab('show');
                $('label[for=inputIssueOn]').addClass('text-error');
            }
            else
            {
                $('label[for=inputIssueOn]').removeClass('text-error');
            }
            if($("#inputIssuePl").val()=='')
            {
                $('#myTabPages a[href="#Logistics"]').tab('show');
                $('label[for=inputIssuePl]').addClass('text-error');
            }
            else
            {
                $('label[for=inputIssuePl]').removeClass('text-error');
            }


            if($("#inputPassNExpires").val()=='')
            {
                $('#myTabPages a[href="#Logistics"]').tab('show');
                $('label[for=inputPassNExpires]').addClass('text-error');
            }
            else
            {
                $('label[for=inputPassNExpires]').removeClass('text-error');
            }

            if($("#inputPlaceOfBirth").val()=='')
            {

                $('#myTabPages a[href="#Logistics"]').tab('show');
                $('label[for=inputPlaceOfBirth]').addClass('text-error');
            }
            else
            {
                $('label[for=inputPlaceOfBirth]').removeClass('text-error');
            }

        })


        $("#inputIssueOn").datepicker({

            defaultDate: "+1w",

            dateFormat: "yy-mm-dd",

            changeMonth: true,

            numberOfMonths: 1

        });

        $("#inputPassNExpires").datepicker({

            defaultDate: "+1w",

            dateFormat: "yy-mm-dd",

            changeMonth: true,

            numberOfMonths: 1

        });
    });
</script>