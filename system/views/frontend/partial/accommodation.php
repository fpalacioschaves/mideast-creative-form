<div class="control-group">

    <span class="help-block">Does Hivos cover your acommodation? </span>

    <label class="radio" for="ticketOpt1">
        <input id="hivosCoverYourAcommodation-1" type="radio" checked="checked" name="hivosCoverYourAcommodation" value="1">
        Yes
    </label>

    <label class="radio" for="ticketOpt2">
        <input id="hivosCoverYourAcommodation-2" type="radio" checked="checked" name="hivosCoverYourAcommodation" value="0">
        No
    </label>

    <span class="help-block" id="blockData">
      You can stay in the hotel for € 80 per night incl. breakfast. You will pay the costs of your hotel stay at the hotel reception desk, once you have arrived. Our programme team manages your reservation until this time (this way, we can offer our guests a discount), so if you have any questions regarding your stay, please contact us at <a href="mailto:<?php echo get_option('_nrRegEm'); ?>"><?php echo get_option('_nrRegEm'); ?></a> . Please be aware that your reservation is binding: by completing this form, you agree to stay (and pay) the following days:
    </span>

</div>

<div id="hivosCoverYourAcommodationView"></div>
<p>
    <span class="help-block">Please indicate:</span>
</p>

<p>
    <label for="inputDateOfArrival4">Date of arrival</label>
    <input id="inputDateOfArrival4" class="input-xxlarge" name="inputDateOfArrival4" type="text"/>
</p>

<p>
    <label for="inputDateOfDeparture4">Date of departure</label>
    <input id="inputDateOfDeparture4" class="input-xxlarge" name="inputDateOfDeparture4" type="text"/>
</p>

<script type="text/javascript">

    jQuery(document).ready(function ($) {
        $("[name='hivosCoverYourAcommodation']").click(function () {
            if ($(this).val() == '1')
            {
                $("#blockData").html("Hivos covers your stay from October 10 until October 13, 2013. Any more nights are on the participant's own costs: 80 euros per night, incl. breakfast. If applicable, you will pay for this upon arrival in the hotel. Our programme team manages your reservation until this time (this way, we can offer our guests a discount), so if you have any questions regarding your stay, please contact us at <a href=\"mailto:<?php echo get_option('_nrRegEm'); ?>\"><?php echo get_option('_nrRegEm'); ?></a> . Please be aware that your reservation is binding: by completing this form, you agree to stay (and pay) the following days:");
            }
            else
            {
                $("#hivosCoverYourAcommodationView").html('');
                $("#blockData").html('You can stay in the hotel for € 80 per night incl. breakfast. You will pay the costs of your hotel stay at the hotel reception desk, once you have arrived. Our programme team manages your reservation until this time (this way, we can offer our guests a discount), so if you have any questions regarding your stay, please contact us at openforchange@hivos.nl . Please be aware that your reservation is binding: by completing this form, you agree to stay (and pay) the following days:');
            }
        });

        $("#inputDateOfArrival4").datepicker({

            defaultDate: "+1w",

            minDate: new Date(2013,9,01),

            dateFormat: "yy-mm-dd",

            changeMonth: true,

            numberOfMonths: 1,

            onClose: function (selectedDate)
            {
                $("#inputDateOfDeparture4").datepicker("option", "minDate", selectedDate);

                $("#checkIn").html(selectedDate);
            }
        });

        $("#inputDateOfDeparture4").datepicker({

            defaultDate:  new Date(2013,9,01),

            changeMonth: true,

            numberOfMonths: 1,

            dateFormat: "yy-mm-dd",

            onClose: function (selectedDate)
            {
                $("#inputDateOfArrival4").datepicker("option", "maxDate", selectedDate);

                $("#checkOut").html(selectedDate);
            }
        });


    });
</script>