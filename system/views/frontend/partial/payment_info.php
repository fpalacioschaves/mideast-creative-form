<?php if($status == 'completed'): ?>

    <div>
            Thank you very much for your registration, we look forward seeing you at Open for Change! Our programme team will contact you with regards to logistics (visa, transport, reimbursements, etc).
            If you have any questions, please take a look at our Frequently Asked Questions, or e-mail us at <a href="mailto:<?php echo get_option('_nrRegEm'); ?>"><?php echo get_option('_nrRegEm'); ?></a>.
    </div>
<?php else: ?>


    <div><?php echo $message; ?></div>



        <?php endif;?>


