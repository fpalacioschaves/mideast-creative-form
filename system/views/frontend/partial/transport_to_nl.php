<legend>Transport to the Netherlands </legend>
<div class="control-group">
    <div class="control">
            <span class="help-block">
                <p>You are kindly requested to make your own travel arrangements.</p>
    <p>Hivos is able to cover travel and accommodation of a limited number of international participants. Please get in touch with us at <a href="mailto:<?php echo get_option('_nrRegEm'); ?>"><?php echo get_option('_nrRegEm'); ?></a></p>
   <p>Please indicate whether you need a visa to visit the European Union:
       If yes, we get in touch with you to provide you with an official invitation letter and other information, if needed.</p>
    <p>I need a visa to visit the European Union:</p>
            </span>
    </div>
    <label class="radio">
        <input id="visitEUVisa4-1" type="radio" checked="checked" name="visitEUVisa4" value="1">
        Yes
    </label>
    <label class="radio">
        <input id="visitEUVisa4-2" type="radio" checked="checked" name="visitEUVisa4" value="0">
        No
    </label>
    <div id="visitEUVisaInfo"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ($)
    {

        var visitEUVisaInfo = $("#visitEUVisaInfo");

        $("[name='visitEUVisa4']").click(function () {

            if ($(this).val() == '1')
            {

                var data = {action: 'load_need_visa'};

                $.post('/wp-admin/admin-ajax.php', data, function (response) {
                    visitEUVisaInfo.html(response);
                });
            }
            else
            {
                visitEUVisaInfo.html("");
            }
        });

    });
</script>