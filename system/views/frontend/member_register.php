<script>
    jQuery(document).ready(function($) {
        $('body').scrollTo(".site-container");
        //$('.english').addClass('active');

        var language = $("#language_traslation").val();

        if (language == "french") {
            $(".english").removeClass("active");
            $(".french").addClass("active");
            $("#language_traslation").val("french");
        }

        if (language == "english") {
            $(".french").removeClass("active");
            $(".english").addClass("active");
            $("#language_traslation").val("english");
        }

        $("#english").click(function() {

            $(".french").removeClass("active");
            $(".english").addClass("active");
            ("#language_traslation").val("english");
            $("#nextStep2").val("Finish member registration");
        });

        $("#french").click(function() {

            $(".english").removeClass("active");
            $(".french").addClass("active");
            $("#language_traslation").val("french");
            $("#nextStep2").val("Terminer l’inscription des membres");
        });



    });
</script>



<div id="form-container">
    <form class="form-horizontal"  id="formRegister" name="formRegister" method="post">


        <input type="hidden" name="member_register" value="member_register">

        <input type="hidden" name="team_id" value="<?php echo $team_id; ?>">
        <input type="hidden" name="team_name" value="<?php echo $team_name; ?>">
        <input type="hidden" name="number_members" value="<?php echo $number_members; ?>">
        <input type="hidden" name="team_email" value="<?php echo $team_email; ?>">

        <input type="hidden" name="language_traslation" id="language_traslation" value="<?php echo $language_traslation; ?>">



        <div id="persInfo">
            <a href="#" id="english">English</a>
            <a href="#" id="french">French</a>


            <?php for ($i = 1; $i <= $number_members - 1; $i++) { ?>
                <fieldset>
                    <legend class="english">Team Member <?php echo $i; ?> <small class="text-info pull-right"></small></legend>
                    <legend class="french">Membre de l'équipe <?php echo $i; ?> <small class="text-info pull-right"></small></legend>
                </fieldset>

                <div>
                    <label for="full_name" class="full_name<?php echo $i; ?> english">Full name: </label>
                    <label for="full_name" class="full_name<?php echo $i; ?> french">Nom et prénom: </label>
                    <input id="full_name<?php echo $i; ?>"  class="input-xxlarge" type="text" name="full_name<?php echo $i; ?>"  />
                </div>

                <div>
                    <label for="birthdate" class="birthdate<?php echo $i; ?> english">Birth date (dd/mm/yyyy): </label>
                    <label for="birthdate" class="birthdate<?php echo $i; ?> french">Date de naissance (dd/mm/yyyy): </label>
                    <input id="birthdate<?php echo $i; ?>"  class="input-xxlarge" type="text" name="birthdate<?php echo $i; ?>"  />
                </div>

                <div>
                    <label for="email" class="email<?php echo $i; ?> english">Email: </label>
                    <label for="email" class="email<?php echo $i; ?> french">Email: </label>
                    <input id="email<?php echo $i; ?>"  class="input-xxlarge" type="text" name="email<?php echo $i; ?>"  />
                </div>

                <div>
                    <label for="educational_background<?php echo $i; ?>" class="educational_background english">Educational background: </label>
                    <label for="educational_background<?php echo $i; ?>" class="educational_background french">Formation scolaire: </label>
                    <textarea id="educational_background<?php echo $i; ?>"  class="input-xxlarge" type="text" name="educational_background<?php echo $i; ?>" ></textarea>

                </div>

                <div>
                    <label for="current_position" class="current_position<?php echo $i; ?> english">Current job/position/study: </label>
                    <label for="current_position" class="current_position<?php echo $i; ?> french">Emploi actuel/poste/études: </label>
                    <textarea id="current_position<?php echo $i; ?>"  class="input-xxlarge" type="text" name="current_position<?php echo $i; ?>" ></textarea>

                </div>

                <div>
                    <label for="twitter_profile" class="twitter_profile<?php echo $i; ?> english">Twitter profile: </label>
                    <label for="twitter_profile" class="twitter_profile<?php echo $i; ?> french">Twitter profil: </label>
                    <input id="twitter_profile<?php echo $i; ?>"  class="input-xxlarge" type="text" name="twitter_profile<?php echo $i; ?>"  />

                </div>

                <div>
                    <label for="facebook_profile" class="facebook_profile<?php echo $i; ?> english">Facebook profile: </label>
                    <label for="facebook_profile" class="facebook_profile<?php echo $i; ?> french">Facebook profil: </label>
                    <input id="facebook_profile<?php echo $i; ?>"  class="input-xxlarge" type="text" name="facebook_profile<?php echo $i; ?>"  />

                </div>

                <div>
                    <label for="language_skills1" class="language_skills<?php echo $i; ?> english">Language skills (which languages do you speak/understand): </label>
                    <label for="language_skills1" class="language_skills<?php echo $i; ?> french">Langues (quelles langues vous maîtrisez): </label>
                    <input class="my_check" type="checkbox" name="language_skills<?php echo $i; ?>[]"  value="English"><span class="english"> English</span><span class="french"> Anglais</span><br>
                    <input class="my_check" type="checkbox" name="language_skills<?php echo $i; ?>[]"  value="Arabic"><span class="english"> Arabic</span><span class="french"> Arabe</span><br>
                    <input class="my_check" type="checkbox" name="language_skills<?php echo $i; ?>[]"  value="French"><span class="english"> French</span><span class="french"> Français</span><br>
                    <div class="other_language">
                        <span class="english my_check">Other: </span><span class="french my_check">Autre: </span><input id="other_language<?php echo $i; ?>" name="other_language<?php echo $i; ?>"  class="" type="text" name="other_language"  />
                    </div>

                </div>


                <div>
                    <label for="member_type" class="member_type<?php echo $i; ?> english">Position within the team: </label>
                    <label for="member_type" class="member_type<?php echo $i; ?> french">Rôle dans l’équipe: </label>
                    <input id="member_type<?php echo $i; ?>"  class="input-xxlarge" type="text" name="member_type<?php echo $i; ?>"  />

                </div>

            <?php } ?>
            <div>
                <input type="button" class="btn-large btn btn-inverse" id="nextStep2" name="nextStep2" value="Finish member registration" >
            </div>
            <div id="errors_english" class="alert english">

            </div>

            <div id="errors_french" class="alert french">

            </div>
            <div class="english">
                <small>
                    Your personal information will be treated with care and will not be used or disclosed for purposes other than the facilitation of your stay at the event.<br>
                    We will not distribute this content among any third parties other than those assisting Hivos in the organization of this event.
                </small>
            </div>

            <div class="french">
                <small>Les données que vous avez fournies ne seront pas utilisées sauf pour l’objet de ce concours.<br>
                    On ne partagera pas ce contenu avec des tiers, mis à part ceux qui aideront Hivos dans l’organisation de cet évènement. 
                </small>
            </div>
        </div>



    </form>

</div>




<!--</div>-->

