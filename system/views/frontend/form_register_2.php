<div id="form-container">

<form class="form-horizontal" id="formRegister" name="formRegister" method="post">
    <input type="hidden" name="formRegister2">
    <input type="hidden" name="formRegisterOptionChoosed" value="<?echo $choose_option ; ?>">

    <p>On Friday 11 and Saturday 12 October, we will exchange, explore and experiment around the theme of the Open Society. A ticket is required to attend this event.</p>

    <div class="control-group">


        <?php if(isset($_GET["free"]) && $_GET["free"]=='true') :?>


            <label class="radio">
                <input id="ticketOpt4" type="radio" checked="checked" name="ticketOpt" value="0">
                Free of charge (****) € 0
            </label>

            <p>
                (***) A few categories of persons can access Open for Change free of charge; for example, Hivos employees, press, keynote speakers, etc. We like them to register through our website too, therefore this extra category.<br />
            </p>

        <?php else: ?>
        <label class="radio">
            <input id="ticketOpt1" type="radio" checked="checked" name="ticketOpt" value="75">
            Regular fee € 75 ex. VAT (€ 90,75 incl. VAT)
        </label>

        <label class="radio">
            <input id="ticketOpt2" type="radio" name="ticketOpt" value="200">
            Large NGO (*) / Academia € 200 ex. VAT (€ 242,- incl. VAT)
        </label>

        <label class="radio">
            <input id="ticketOpt3" type="radio"  name="ticketOpt" value="300">
            Medium or Large Business (**) & IGO / Developed World Government (***) € 300 ex. VAT (€ 363,- incl. VAT)
        </label>



            <p>
                ( * ): > 4Mio EUR turnover<br />
                (**): over 15 staff or > 1Mio EUR turnover<br />
                (***):if you work for the government but are coming in your own personal capacity or are self-funding you qualify for the Regular fee<br />
                <br />
                We want to ensure as many people as possible can participate in the Open for Change Event. If a discount is required  for you to be able to participate, you are requested to email us at <a href="mailto:<?php echo get_option('_nrRegEm'); ?>"><?php echo get_option('_nrRegEm'); ?></a> with the reason and we’ll do our best to accommodate you.<br />
                At the same time we should emphasize that we are a non-profit organization and the event is run on a break-even basis with ticket sales being an essential aspect of covering the costs of the event.<br />
                This fee covers full access to the event, meals and drinks and an event package.
            </p>
        <?php endif ; ?>



        <label class="checkbox">
            <input id="supportOpt" type="checkbox"  name="supportOpt" value="1">
            I would like to support a (foreign) participant in less fortunate financial circumstances -> € 75 ex. VAT (€ 90,75 incl. VAT)
        </label>






    </div>




    <p>
        <input type="button" class="btn-large btn btn-inverse" id="nextStep" name="nextStep" value="Next Step" >
    </p>



</form>
</div>
