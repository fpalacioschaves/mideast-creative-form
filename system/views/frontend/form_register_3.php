<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#idea_description').simplyCountable({
            counter: '#counter',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#idea_description').simplyCountable({
            counter: '#counterf',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#how_far').simplyCountable({
            counter: '#counter2',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#how_far').simplyCountable({
            counter: '#counter2f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#experience').simplyCountable({
            counter: '#counter3',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#experience').simplyCountable({
            counter: '#counter3f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#motivation').simplyCountable({
            counter: '#counter4',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#motivation').simplyCountable({
            counter: '#counter4f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        $('#hope_to_gain').simplyCountable({
            counter: '#counter5',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#hope_to_gain').simplyCountable({
            counter: '#counter5f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#problem_to_solve').simplyCountable({
            counter: '#counter6',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#problem_to_solve').simplyCountable({
            counter: '#counter6f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#how_idea_solve').simplyCountable({
            counter: '#counter7',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
         $('#how_idea_solve').simplyCountable({
            counter: '#counter7f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('#target_customer').simplyCountable({
            counter: '#counter8',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });
        
        $('#target_customer').simplyCountable({
            counter: '#counter8f',
            countType: 'words',
            maxCount: 300,
            strictMax: true,
            countDirection: 'down'
        });

        $('.english').addClass('active');


        $("#english").click(function() {
            $("#language_traslation").val("english");
            $("#nextStep").val("Continue registration");

        });

        $("#french").click(function() {
            $("#language_traslation").val("french");
            $("#nextStep").val("Continuer votre inscription");

        });
    });
</script>



<div id="form-container">
    <form class="form-horizontal"  id="formRegister" name="formRegister" method="post">

        <input type="hidden" name="formRegister3" value="formRegister3">

        <input type="hidden" name="language_traslation" id="language_traslation" value="english">


        <div id="persInfo">
            <a href="#" id="english">English</a>
            <a href="#" id="french">French</a>

            <fieldset>
                <legend class="english">General information:<small class="text-info pull-right"></small></legend>
                <legend class="french">Informations Générales:<small class="text-info pull-right"></small></legend>
            </fieldset>


            <div>
                <label for="full_name" class="full_name english">Full name: </label>
                <label for="full_name" class="full_name french">Nom et Prénom: </label>
                <input id="full_name"  class="input-xxlarge" type="text" name="full_name"  />
            </div>

            <div>
                <label for="birthdate" class="birthdate english">Birth date (dd/mm/yyyy): </label>
                <label for="birthdate" class="birthdate french">Date de naissance (dd/mm/yyyy): </label>
                <input id="birthdate"  class="input-xxlarge" type="text" name="birthdate"  />
            </div>

            <div>
                <label for="address" class="address english">Address: </label>
                <label for="address" class="address french">Adresse: </label>
                <textarea id="address"  class="input-xxlarge"  name="address" ></textarea>

            </div>

            <div>
                <label for="phone_number" class="phone_number english">Phone number: </label>
                <label for="phone_number" class="phone_number french">Numéro de téléphone: </label>
                <input id="phone_number"  class="input-xxlarge" type="text" name="phone_number"  />
            </div>

            <div>
                <label for="email" class="email english">Email: </label>
                <label for="email" class="email french">Email: </label>
                <input id="email"  class="input-xxlarge" type="text" name="email"  />
            </div>

            <div>
                <label for="educational_background" class="educational_background english">Educational background: </label>
                <label for="educational_background" class="educational_background french">Formation scolaire: </label>
                <textarea id="educational_background"  class="input-xxlarge"  name="educational_background" ></textarea>

            </div>

            <div>
                <label for="current_job" class="current_job english">Current job/position/study: </label>
                <label for="current_job" class="current_job french">Emploi actuel/poste/études: </label>
                <textarea id="current_job"  class="input-xxlarge"  name="current_job" ></textarea>

            </div>

            <div>
                <label for="twitter_profile" class="twitter_profile english">Twitter profile: </label>
                <label for="twitter_profile" class="twitter_profile french">Twitter profil: </label>  
                <input id="twitter_profile"  class="input-xxlarge" type="text" name="twitter_profile"  />

            </div>

            <div>
                <label for="facebook_profile" class="facebook_profile english">Facebook profile: </label>
                <label for="facebook_profile" class="facebook_profile french">Facebook profil: </label>
                <input id="facebook_profile"  class="input-xxlarge" type="text" name="facebook_profile"  />

            </div>

            <div>
                <label for="language_skills1" class="language_skills english">Language skills (which languages do you speak/understand): </label>
                <label for="language_skills1" class="language_skills french">Langues (quelles langues vous maîtrisez): </label>
                <input class="my_check" type="checkbox" name="language_skills[]" id="language_skills1" value="English"><span class="english"> English</span><span class="french"> Anglais</span><br>
                <input class="my_check" type="checkbox" name="language_skills[]" id="language_skills2" value="Arabic"><span class="english"> Arabic</span><span class="french"> Arabe</span><br>
                <input class="my_check" type="checkbox" name="language_skills[]" id="language_skills3" value="French"><span class="english"> French</span><span class="french"> Français</span><br>
                <div class="other_language">
                    <span class="english my_check">Other: </span><span class="french my_check">Autre: </span><input id="other_language"  class="" type="text" name="other_language"  />
                </div>


            </div>


            <fieldset> 
                <legend class="english">Motivation:</legend>
                <legend class="french">Motivation:</legend>
            </fieldset> 


            <div>
                <label for="motivation" class="motivation english">What is your motivation to join this training programme? </label>
                <label for="motivation" class="motivation french">Quelle est votre motivation pour participer à ce programme? </label>
                <textarea id="motivation" class="input-xxlarge" name="motivation" ></textarea>
                <p class="counter english">You have <span id="counter4"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counter4f"></span> mots.</p>
            </div>

            <div>
                <label for="hope_to_gain" class="hope_to_gain english">What do you hope to gain from this training?</label>
                <label for="hope_to_gain" class="hope_to_gain french">Quelles sont vos attentes par rapport à ce programme?</label>
                <textarea id="hope_to_gain" class="input-xxlarge" name="hope_to_gain" ></textarea>
                <p class="counter english">You have <span id="counter5"></span> words left.</p>
                 <p class="counter french">Il vous reste <span id="counter5f"></span> mots.</p>
            </div>


            <fieldset> 
                <legend class="english">Business background:</legend>
                <legend class="french">Business background french no translated:</legend>
            </fieldset> 

            <div>
                <label for="experience" class="experience english">Could you give us a short summary or your previous experiences with having a business/setting up a start-up?</label>
                <label for="experience" class="experience french">En quelques mots, quelle est votre expérience précédente dans la mise en place d’une entreprise ou d’une start-up?</label>
                <textarea id="experience" class="input-xxlarge" name="experience" ></textarea>
                <p class="counter english">You have <span id="counter3"></span> words left.</p>
                 <p class="counter french">Il vous reste <span id="counter3f"></span> mots.</p>
            </div>

            <div class="control-group">

                <span class="help-block english">Have you ever attended or participated in acceleration programmes or entrepreneurship related trainings?</span>
                <span class="help-block french">Avez-vous déjà participé à un programme d'accélération ou à une formation sur l’entrepreneuriat?</span>

                <label class="radio">
                    <input  id="ticketOpt1" type="radio" checked="checked" name="other_participations" value="yes">
                    <span class="english">Yes</span>
                    <span class="french">Oui</span>
                </label>


                <label class="radio">
                    <input  id="ticketOpt2" type="radio" checked="checked" name="other_participations" value="no">
                    <span class="english">No</span>
                    <span class="french">Non</span>
                </label>


            </div>

            <div>
                <label for="wich_participations" class="wich_participations english">If so which one(s):</label>
                <label for="wich_participations" class="wich_participations french">Si oui, le(s)quel(s) ?</label>
                <textarea id="wich_participations" class="input-xxlarge" name="wich_participations" ></textarea>

            </div>


      


            <div class="control-group">

                <span class="help-block english">Are you currently running a start-up/business?</span>
                <span class="help-block french">Gérez-vous une start-up actuellement ?</span>

                <label class="radio">
                    <input  id="ticketOpt5" type="radio" checked="checked" name="current_start_up" value="yes">
                    <span class="english">Yes</span>
                    <span class="french">Oui</span>
                </label>


                <label class="radio">
                    <input id="ticketOpt6" type="radio" checked="checked" name="current_start_up" value="no">
                    <span class="english">No</span>
                    <span class="french">Non</span>
                </label>


            </div>

            <div>
                <label for="name_your_business" class="name_your_business english">What is the name of your business (if applicable)?</label>
                <label for="name_your_business" class="name_your_business french">Si oui, quel est le nom de votre entreprise ?</label>
                <input id="name_your_business"  class="input-xxlarge" type="text" name="name_your_business"  />
            </div>

            <div class="control-group">

                <span class="help-block english">Are you the founder/cofounder of that business (if applicable)?</span>
                <span class="help-block french">Êtes-vous le fondateur/ co-fondateur de cette entreprise ?</span>
                <label class="radio">
                    <input id="ticketOpt7" type="radio" checked="checked" name="founder_cofounder" value="not_applicable">
                    <span class="english">Not applicable</span>
                    <span class="french">Pas applicable</span>
                </label>
                <label class="radio">
                    <input id="ticketOpt8" type="radio"  name="founder_cofounder" value="founder">
                    <span class="english">Founder</span>
                    <span class="french">Fondateur</span>
                </label>


                <label class="radio">
                    <input id="ticketOpt9" type="radio"  name="founder_cofounder" value="cofounder">
                    <span class="english">Cofounder</span>
                    <span class="french">Co-fondateur</span>
                </label>


            </div>



            <fieldset> 
                <legend class="english">Idea:</legend>
                <legend class="french">L’Idée:</legend>
            </fieldset> 

            <div>
                <label for="idea_description" class="idea_description english">Describe in short your idea for this programme:</label>
                <label for="idea_description" class="idea_description french">Quelle est votre idée pour ce programme?</label>
                <textarea id="idea_description" class="input-xxlarge" name="idea_description"  ></textarea>
                <p class="counter english">You have <span id="counter"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counterf"></span> mots.</p>
                



            </div>

            <div>
                <label for="how_far" class="how_far english">How far are you with implementing your idea? </label>
                <label for="how_far" class="how_far french">Quel est le stade de la mise en oeuvre de cette idée?</label>
                <textarea id="how_far" class="input-xxlarge" name="how_far" ></textarea>
                <p class="counter english">You have <span id="counter2"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counter2f"></span> mots.</p>
            </div>

            <div>
                <label for="problem_to_solve" class="problem_to_solve english">What problem you are attempting to solve with your idea?</label>
                <label for="problem_to_solve" class="problem_to_solve french">Quel est le problème que vous tentez résoudre avec cette idée ?</label>
                <textarea id="problem_to_solve" class="input-xxlarge" name="problem_to_solve" ></textarea>
                <p class="counter english">You have <span id="counter6"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counter6f"></span> mots.</p>
            </div>

            <div>
                <label for="how_idea_solve" class="how_idea_solve english">How will your idea solve this problem?</label>
                <label for="how_idea_solve" class="how_idea_solve french">Comment allez-vous résoudre ce problème avec l’idée ?</label>
                <textarea id="how_idea_solve" class="input-xxlarge" name="how_idea_solve" ></textarea>
                <p class="counter english">You have <span id="counter7"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counter7f"></span> mots.</p>
            </div>

            <div>
                <label for="target_customer" class="target_customer english">Who is/are your target customer(s)?</label>
                <label for="target_customer" class="target_customer french">Qui sont vos clients cibles ?</label>
                <textarea id="target_customer" class="input-xxlarge" name="target_customer" ></textarea>
                <p class="counter english">You have <span id="counter8"></span> words left.</p>
                <p class="counter french">Il vous reste <span id="counter8f"></span> mots.</p>
            </div>

            <div>
                <label for="full_name" class="english">How many other team members do you have? : </label>
                <label for="full_name" class="french">Combien de membres font partie de votre équipe ? : </label>
                <SELECT NAME="number_members"  id="number_members">

                    <OPTION VALUE="1" selected="selected">0</OPTION>
                    <OPTION VALUE="2">1</OPTION>
                    <OPTION VALUE="3">2</OPTION>
                    <OPTION VALUE="4">3</OPTION>
                    <OPTION VALUE="5">4</OPTION>
                    <OPTION VALUE="6">5</OPTION>
                    <OPTION VALUE="7">6</OPTION>
                    <OPTION VALUE="8">7</OPTION>
                    <OPTION VALUE="9">8</OPTION>
                    <OPTION VALUE="10">9</OPTION>
                </SELECT> 
    <!--            <input id="number_members"  class="input-xxlarge" type="text" name="number_members"  />-->
            </div>


            <div>
                <input type="button" class="btn-large btn btn-inverse" id="nextStep" name="nextStep" value="Continue registration" >
            </div>
            <div id="errors_english" class="alert english">

            </div>

            <div id="errors_french" class="alert french">

            </div>


            <div class="english">
                <small>
                    Your personal information will be treated with care and will not be used or disclosed for purposes other than the facilitation of your stay at the event.<br>
                    We will not distribute this content among any third parties other than those assisting Hivos in the organization of this event.
                </small>
            </div>
            
            <div class="french">
                <small>Les données que vous avez fournies ne seront pas utilisées sauf pour l’objet de ce concours.<br>
                On ne partagera pas ce contenu avec des tiers, mis à part ceux qui aideront Hivos dans l’organisation de cet évènement. 
                </small>
            </div>
        </div>
    </form>
</div>





<!--</div>-->

