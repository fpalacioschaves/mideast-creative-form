function IsJsonString(str)
{
    try
    {
        JSON.parse(str);
    }
    catch (e)
    {
        return false;
    }
    return true;
}


jQuery(document).ready(function($) {
    // Code using $ as usual goes here.

    $("#english").click(function() {
     
         $(".french").removeClass("active");
         $(".english").addClass("active");
    });

$("#french").click(function() {

         $(".english").removeClass("active");
         $(".french").addClass("active");
    });


    var $create_button = $("#nextStep");
    var $form = $("#formRegister");


    $("#nextStep").click(function() {
       
        var data = {
            action: 'register_process',
            data_to_send: $form.serialize()
        };


        $create_button.attr("disabled", "disabled");

        $.post(MyAjax.ajaxurl, data, function(response)
        {
//            alert("success");
        })
                .done(function(response)
                {
//                    alert("second success");
                })
                .fail(function(response)
                {
//                    alert("error");
                })
                .always(function(response)
                {
                    $("#errors_english").html("");
                     $("#errors_french").html("");
//                   alert("finished");
                    $create_button.removeAttr("disabled");



                    if (IsJsonString(response))
                    {
                        var data = jQuery.parseJSON(response);
//                        alert(data);
                         $("label").removeClass("red");
                         $("input").removeClass("red");
                        if (data.errorElements)
                        {
                            $("#errors_english").append('<a class="close" data-dismiss="alert">×</a>');
                            $("#terr").remove();
                            for (var el in data.errorElements)
                            {
                                    

                                if (data.errorElements[el].label != "")
                                {
//                                    alert(data.errorElements[el].label);
                                    $("."+ data.errorElements[el].label).addClass("red");   
                                    $("#"+ data.errorElements[el].label).addClass("red");   
                                    $("#errors_english").append("<span class=''>The field " + data.errorElements[el].message + " is required</span><br>");
                                    $("#errors_french").append("<span class=''>Domaine " + data.errorElements[el].message_french + " est requis</span><br>");
                                    data.errorElements[el] = "";

                                }
                            }
                            while (data.errorElements.length > 0) {
                                data.errorElements.pop();
                            }
                            if (data.scrollTo)
                            {
                                $('body').scrollTo(data.scrollTo);
                            }

                        }
                        else if (data.success)
                        {
                            $("#form-container").html(data.success);
                        }
                    }
                    else
                    {
                        $("#form-container").html(response);
                    }





                });

    });





});

jQuery(document).on("click", "#nextStep2", function($) {
    var jQuerycreate_button = jQuery("#nextStep2");
    var jQueryform = jQuery("#formRegister");
//     alert("PULSADO"); 
    var data = {
        action: 'register_process',
        data_to_send: jQueryform.serialize()
    };


    jQuerycreate_button.attr("disabled", "disabled");

    jQuery.post(MyAjax.ajaxurl, data, function(response)
    {
//            alert("success");
    })
            .done(function(response)
            {
//                    alert("second success");
            })
            .fail(function(response)
            {
//                    alert("error");
            })
            .always(function(response)
            {
                jQuery("#errors_english").html("");
                jQuery("#errors_french").html("");
//                   alert("finished");
                jQuerycreate_button.removeAttr("disabled");
//                    if (data.scrollTo)
//                            {
//                                $('body').scrollTo(data.scrollTo);
//                            }

                if (IsJsonString(response))
                {
                    
                    var data = jQuery.parseJSON(response);
                   jQuery("label").removeClass("red");
//                   alert(data.errorElements);
                    if (data.errorElements)
                    {
                        jQuery("#errors_english").append('<a class="close" data-dismiss="alert">×</a>');
                        jQuery("#errors_french").append('<a class="close" data-dismiss="alert">×</a>');
                        jQuery("#terr").remove();
                        for (var el in data.errorElements)
                        {

                            if (data.errorElements[el].label != "")
                            {
                                jQuery("."+ data.errorElements[el].label).addClass("red"); 
                                jQuery("#"+ data.errorElements[el].label).addClass("red"); 
                                jQuery("#errors_english").append("<span class=''>The field " + data.errorElements[el].message + " is required</span><br>");
                                jQuery("#errors_french").append("<span class=''>Domaine " + data.errorElements[el].message_french + " est requis</span><br>");
                                data.errorElements[el] = "";

                            }
                        }
                        while (data.errorElements.length > 0) {
                            data.errorElements.pop();
                        }
                        if (data.scrollTo)
                        {
                            jQuery('body').scrollTo(data.scrollTo);
                        }

                    }
                    else if (data.success)
                    {
                        jQuery("#form-container").html(data.success);
                    }
                }
                else
                {
                    jQuery("#form-container").html(response);
                }





            });

});


        
        