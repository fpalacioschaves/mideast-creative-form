<?php
if(! class_exists('MEC_Core')):

class MEC_Core
{

    /***
     *
     * This function is used for helpers initialization files.
     * OBS: all the files should be placed in system/helpers folder and a file should start with  DEB_helpers_[file name].php.
     * Function can be used as this   $this->initHelper('view') ; where view is DEB_helpers_view.php from system/helpers folder.
     * To call a function from that helper ones initialized we use as this: $this->view->[function name] so the structure is:
     * $this->[helper name without DEB_helpers_]->[function name]
     *
     * @access	public
     * @param	string
     * @param	string, array
     * @return	string
     */
    public function initHelper($helper_name=null, $args=null)
    {

            //Check for non null parameter
            if($helper_name)
            {
                    $helper_file_name                = MEC_PATH."/system/helpers/MEC_helpers_{$helper_name}.php" ;
                    //Check if helper file exist
                    if(is_file($helper_file_name))
                    {
                        ob_start();

                            require_once( $helper_file_name);

                         ob_get_clean();

                            //Store required class name in a variable
                            $class_helper_name      = "MEC_helpers_". $helper_name ;

                            //Init the new helper class name
                            if($args)
                            {
                                $new                    = new $class_helper_name($args);
                            }
                            else
                            {
                                $new                    = new $class_helper_name;
                            }

                            /***
                             * Create dynamically Helper class variable.
                             * So then ones initHelper function initialize the class
                             * then we can call any function from that helper class as this
                             * this->[helper name]->[function name]
                             * */
                            $this->{$helper_name}   = $new;

                    }

            }

    }



    public function initLibrary($library_name=null, $args=null)
    {

        //Check for non null parameter
        if($library_name)
        {
            $library_file_name                = MEC_PATH."/system/library/{$library_name}.php" ;
            //Check if helper file exist
            if(is_file($library_file_name))
            {
                ob_start();

                require_once( $library_file_name);

                ob_get_clean();

                //Store required class name in a variable
                $class_library_name      =  $library_name ;

                //Init the new helper class name
                if($args)
                {
                    $new                    = new $class_library_name($args);
                }
                else
                {
                    $new                    = new $class_library_name;
                }

                /***
                 * Create dynamically Helper class variable.
                 * So then ones initHelper function initialize the class
                 * then we can call any function from that helper class as this
                 * this->[helper name]->[function name]
                 * */
                $this->{$library_name}   = $new;

            }

        }

    }

    /***
     *
     * This function is used for models initialization files.
     * OBS: all the files should be placed in system/models folder
     * Function can be used as this   $this->initModel('view') ;
     * To call a function from that model ones initialized we use as this: $this->view->[function name] so the structure is:
     * $this->[model name ]->[function name]
     *
     * @access	public
     * @param	string
     * @param	string, array
     * @return	string
     */
    public function initModel($model_name=null, $args=null)
    {

            //Check for non null parameter
            if($model_name)
            {
                    $model_file_name                = MEC_PATH."/system/models/{$model_name}.php" ;
                    //Check if helper file exist
                    if(is_file($model_file_name))
                    {
                        ob_start();

                            require_once( $model_file_name);

                         ob_get_clean();

                            //Store required class name in a variable
                            $class_helper_name      =  $model_name ;

                            //Init the new helper class name
                            if($args)
                            {
                                $new                    = new $class_helper_name($args);
                            }
                            else
                            {
                                $new                    = new $class_helper_name;
                            }

                            /***
                             * Create dynamically Helper class variable.
                             * So then ones initHelper function initialize the class
                             * then we can call any function from that helper class as this
                             * this->[helper name]->[function name]
                             * */
                            $this->{$model_name}   = $new;

                    }

            }

    }

}

endif;