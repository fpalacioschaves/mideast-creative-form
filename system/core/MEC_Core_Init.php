<?php

if (!class_exists('MEC_Core_Init')):

    class MEC_Core_Init extends MEC_Core {

        private $paymen_item_ID;

        public function __construct() {
            //add your actions to the constructor!

            add_shortcode('webberty-form', array(&$this, 'MEC_webberty_form_short_code'));

            add_shortcode('webberty-team-member', array(&$this, 'MEC_webberty_team_member_short_code'));

            add_action('admin_menu', array(&$this, 'MECRegisterMenu'));

            add_action('wp_enqueue_scripts', array(&$this, 'add_theme_styles'));

            add_action('wp_enqueue_scripts', array(&$this, 'add_theme_scripts'));

            add_action('wp_ajax_register_process', array(&$this, 'register_ajax_process'));

            add_action('wp_ajax_nopriv_register_process', array(&$this, 'register_ajax_process'));

            add_action('wp_ajax_remove_normal_reg_item', array(&$this, 'remove_normal_reg_item'));

            add_action('wp_ajax_nopriv_remove_normal_reg_item', array(&$this, 'remove_normal_reg_item'));

            add_action('wp_ajax_mark_normal_reg_item', array(&$this, 'mark_normal_reg_item'));

            add_action('wp_ajax_nopriv_mark_normal_reg_item', array(&$this, 'mark_normal_reg_item'));

            add_action('wp_ajax_save_settings', array(&$this, 'MEC_ajax_save_settings'));

            add_action('wp_ajax_nopriv_save_settings', array(&$this, 'MEC_ajax_save_settings'));

            add_action('wp_ajax_live_email_check', array(&$this, 'MEC_ajax_live_email_check'));

            add_action('wp_ajax_nopriv_live_email_check', array(&$this, 'MEC_ajax_live_email_check'));

            add_action('wp_ajax_export_normal_item', array(&$this, 'export_normal'));


            add_action('wp_ajax_nopriv_export_normal_item', array(&$this, 'export_normal'));



            add_action('wp_ajax_update_normal', array(&$this, 'update_normal_function'));

            add_action('wp_ajax_nopriv_update_normal', array(&$this, 'update_normal_function'));

            /*             * *
             * MEC_views_menu  class initialization.
             * This class contain all functions related with view templates
             */
            $this->initHelper('view');

            $this->initHelper('input');

            $this->initModel('register');
        }

        public function update_normal_function() {

            if ($this->input->is_ajax_request()):

                $post = $this->input->post();

                $form_data = $post['data_to_send'];

                parse_str($form_data, $form_elements);

                if ($this->register->update_normal_form($form_elements)) {
                    echo '<p id="infoResult"><div id="setting-error-settings_updated" class="updated settings-error"><p><strong>Item successfully updated</strong></p></div></p>';
                } else {
                    echo '<p id="infoResult"><div id="setting-error-settings_updated" class="updated settings-error"><p><strong>Nothing to update.</strong></p></div></p>';
                }

            endif;


            wp_die();
        }

        /*         * *
         *  This option it remove items from "Normal Register" menu clicking on small "Trash" icon
         * @access	public
         * @return	string
         */

        public function remove_normal_reg_item() {

            if ($this->input->is_ajax_request()):

                $post = $this->input->post();

                $item_id = $post['item_id'];

                if ($this->register->removeItem('normal', $item_id)) {
                    $this->view->logData("Normal item " . $item_id . " successfully removed");
                    echo "OK";
                } else {
                    echo "ERROR";
                    $this->view->logData("Error removing normal item " . $item_id);
                }
            endif;
            wp_die();
        }

        /*         * *
         *  This option it marks as viewed/unviewed items from "Normal Register" menu clicking on small "Eye" icon via ajax
         * @access	public
         * @return	string
         */

        public function mark_normal_reg_item() {

            if ($this->input->is_ajax_request()):

                $post = $this->input->post();

                $item_id = $post['item_id'];

                $value_data1 = $post['value_data'];

                if ($this->register->markItemAsReaded('normal', $item_id, $value_data1)) {
                    echo $value_data1;
                } else {
                    $this->view->logData("Error Marking normal item " . $item_id);
                }


            endif;
            wp_die();
        }

        /**
         * We have to check if the email is already registered
         * @access public
         * @return string
         */
        public function MEC_ajax_live_email_check() {
            $post = $this->input->post();
            if ($this->register->checkPaymentEmailRegister($post['inputYourEmail4'])) {
                echo 'NO';
            }

            wp_die();
        }

        /*         * *
         *  This function it will add normal form register data from frontend
         * @access	public
         * @return	string
         */

        public function register_ajax_process() {
            if ($this->input->is_ajax_request()):
                $result = array();
                $post = $this->input->post();

                $form_data = $post['data_to_send'];

                parse_str($form_data, $form_elements);

                $from_email = "kevcoy9@gmail.com";

                if (isset($form_elements['formRegister3'])) {


//                    wp_die();
                    $this->view->logData("User start normal process");



                    $data = null;
                    $error = 0;
//                    if (empty($form_elements['team_name'])) {
//                        $error = 1;
//                        // $result = array("team_name" => ' *Required ');
//                        array_push($result, "Team name");
//                        $this->view->logData("User tried to register with empty field: team_name");
//                    }
                    if (empty($form_elements['idea_description'])) {
                        $error = 1;
                        array_push($result, array('label' => "idea_description", "message" => "Describe in short your idea for this programme", "message_french" => "Quelle est votre idée pour ce programme"));
                        //array_push($result, array("idea_description" => ' *Required '));
                        // $result = array("idea_description" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: idea_description");
                    }
                    if (empty($form_elements['how_far'])) {
                        $error = 1;
                        array_push($result, array('label' => "how_far", "message" => "How far are you with implementing your idea ", "message_french" => "Quel est le stade de la mise en oeuvre de cette idée "));

                        // $result = array("how_far" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: how_far");
                    }
                    if (empty($form_elements['problem_to_solve'])) {
                        $error = 1;

                        array_push($result, array('label' => "problem_to_solve", "message" => "What problem you are attempting to solve with your idea?", "message_french" => "Quel est le problème que vous tentez résoudre avec cette idée?"));

                        //$result = array("problem_to_solve" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: problem_to_solve");
                    }

                    if (empty($form_elements['how_idea_solve'])) {
                        $error = 1;

                        array_push($result, array('label' => "how_idea_solve", "message" => "How will your idea solve this problem?", "message_french" => "Comment allez-vous résoudre ce problème avec l’idée ?"));

                        //$result = array("how_idea_solve" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: how_idea_solve");
                    }

                    if (empty($form_elements['target_customer'])) {
                        $error = 1;
                        array_push($result, array('label' => "target_customer", "message" => "Who is/are your target customer(s)?", "message_french" => "Qui sont vos clients cibles ?"));

                        //$result = array("target_customer" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: target_customer");
                    }
                    if (empty($form_elements['experience'])) {
                        $error = 1;
                        array_push($result, array('label' => "experience", "message" => "Could you give us a short summary or your previous experiences with having a business/setting up a start-up ?", "message_french" => "En quelques mots, quelle est votre expérience précédente dans la mise en place d’une entreprise ou d’une start-up?"));

                        //$result = array("experience" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: experience");
                    }

                    if (empty($form_elements['motivation'])) {
                        $error = 1;
                        array_push($result, array('label' => "motivation", "message" => "What is your motivation to join this training programme?", "message_french" => "Quelle est votre motivation pour participer à ce programme?"));

                        //$result = array("motivation" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: motivation   ");
                    }

                    if (empty($form_elements['hope_to_gain'])) {
                        $error = 1;
                        array_push($result, array('label' => "hope_to_gain", "message" => "What do you hope to gain from this training?", "message_french" => "Quelles sont vos attentes par rapport à ce programme?"));

                        //$result = array("hope_to_gain" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: hope_to_gain   ");
                    }

                    if (empty($form_elements['full_name'])) {
                        $error = 1;
                        array_push($result, array('label' => "full_name", "message" => "Full name", "message_french" => "Nom et Prénom"));

                        //$result = array("full_name" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: full_name");
                    }
                    if (empty($form_elements['birthdate'])) {
                        $error = 1;
                        array_push($result, array('label' => "birthdate", "message" => "Birth date", "message_french" => "Date de naissance"));

                        // $result = array("birthdate" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: birthdate");
                    }
                    if (empty($form_elements['address'])) {
                        $error = 1;
                        array_push($result, array('label' => "address", "message" => "Address", "message_french" => "Adresse"));

                        //$result = array("address" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: address");
                    }
                    if (empty($form_elements['phone_number'])) {
                        $error = 1;
                        array_push($result, array('label' => "phone_number", "message" => "Phone number", "message_french" => "Numéro de téléphone"));

                        //$result = array("phone_number" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: phone_number");
                    }

                    if (empty($form_elements['email'])) {
                        $error = 1;
                        array_push($result, array('label' => "email", "message" => "Email", "message_french" => "Email"));

                        //$result = array("email" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: email");
                    }

                    if (empty($form_elements['educational_background'])) {
                        $error = 1;
                        array_push($result, array('label' => "educational_background", "message" => "Educational background", "message_french" => "Formation scolaire"));

                        //$result = array("educational_background" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: educational_background");
                    }
                    if (empty($form_elements['current_job'])) {
                        $error = 1;
                        array_push($result, array('label' => "current_job", "message" => "Current job/position/study", "message_french" => "Emploi actuel/poste/études"));

                        //$result = array("current_job" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: current_job");
                    }
                    //if (empty($form_elements['language_skills']) && empty($form_elements['other_language'])) {
                    if (empty($form_elements['language_skills'])) {
                        $error = 1;
                        array_push($result, array('label' => "language_skills", "message" => "Language skills (which languages do you speak/understand): ", "message_french" => "Langues (quelles langues vous maîtrisez)"));

                        // $result = array("language_skills" => ' *Required ');
                        $this->view->logData("User tried to register with empty field: language_skills");
                    }

                    if ($error == 0) {
                        if ($this->register->addNormalRegisterItem($form_elements) && $this->register->addNormalRegisterMemberCoreItem($form_elements)) {
                            $this->view->logData("Normal item successfully registered");
                            if (isset($form_elements['language_skills'])) {
                                $language_skills = implode(",", $form_elements['language_skills']);
                            } else {
                                $language_skills = "";
                            }

                            $message = '<html><body>';
                            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                            //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                            $message .= "<tr><td><strong>Full name:</strong> </td><td>" . strip_tags($form_elements['full_name']) . "</td></tr>";
                            $message .= "<tr><td><strong>Birth date:</strong> </td><td>" . strip_tags($form_elements['birthdate']) . "</td></tr>";
                            $message .= "<tr><td><strong>Address :</strong> </td><td>" . strip_tags($form_elements['address']) . "</td></tr>";
                            $message .= "<tr><td><strong>Phone number</strong> </td><td>" . strip_tags($form_elements['phone_number']) . "</td></tr>";
                            $message .= "<tr><td><strong>Email</strong> </td><td>" . strip_tags($form_elements['email']) . "</td></tr>";
                            $message .= "<tr><td><strong>Educational background</strong> </td><td>" . strip_tags($form_elements['educational_background']) . "</td></tr>";
                            $message .= "<tr><td><strong>Current job/position/study</strong> </td><td>" . strip_tags($form_elements['current_job']) . "</td></tr>";
                            $message .= "<tr><td><strong>Twitter profile </strong> </td><td>" . strip_tags($form_elements['twitter_profile']) . "</td></tr>";
                            $message .= "<tr><td><strong>Facebook profile </strong> </td><td>" . strip_tags($form_elements['facebook_profile']) . "</td></tr>";
                            $message .= "<tr><td><strong> Language skills (which languages do you speak/understand): </strong> </td><td>" . $language_skills . "</td></tr>";
                            $message .= "<tr><td><strong> Other language: </strong> </td><td>" . strip_tags($form_elements['other_language']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Member type</strong> </td><td>Core member</td></tr>";
                            //$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                            //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                            $message .= "<tr><td><strong>Team Name:</strong> </td><td>" . strip_tags($form_elements['full_name']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Could you give us a short summary or your previous experiences with having a business/setting up a start-up (use max. 300 words)?</strong> </td><td>" . strip_tags($form_elements['experience']) . "</td></tr>";
                            $message .= "<tr><td><strong>What is your motivation to join this training programme? </strong> </td><td>" . strip_tags($form_elements['motivation']) . "</td></tr>";
                            $message .= "<tr><td><strong>What do you hope to gain from this training?</strong> </td><td>" . strip_tags($form_elements['hope_to_gain']) . "</td></tr>";
                            $message .= "<tr><td><strong>Have you ever attended or participated in acceleration programmes or entrepreneurship related trainings?</strong> </td><td>" . strip_tags($form_elements['other_participations']) . "</td></tr>";
                            $message .= "<tr><td><strong>If so which one(s):</strong> </td><td>" . strip_tags($form_elements['wich_participations']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Have you ever run a start-up or business?</strong> </td><td>" . strip_tags($form_elements['run_start_up']) . "</td></tr>";
                            //$message .= "<tr><td><strong>If so how many?</strong> </td><td>" . strip_tags($form_elements['how_many_start_up']) . "</td></tr>";
                            $message .= "<tr><td><strong>Are you currently running a start-up/business?</strong> </td><td>" . strip_tags($form_elements['current_start_up']) . "</td></tr>";
                            $message .= "<tr><td><strong>What is the name of your business (if applicable)?</strong> </td><td>" . strip_tags($form_elements['name_your_business']) . "</td></tr>";
                            $message .= "<tr><td><strong>Are you the founder/cofounder of that business (if applicable)?</strong> </td><td>" . strip_tags($form_elements['founder_cofounder']) . "</td></tr>";

                            $message .= "<tr><td><strong>Describe in short your idea for this programme:</strong> </td><td>" . strip_tags($form_elements['idea_description']) . "</td></tr>";
                            $message .= "<tr><td><strong>How far are you with implementing your idea :</strong> </td><td>" . strip_tags($form_elements['how_far']) . "</td></tr>";
                            $message .= "<tr><td><strong>What problem you are attempting to solve with your idea?</strong> </td><td>" . strip_tags($form_elements['problem_to_solve']) . "</td></tr>";
                            $message .= "<tr><td><strong>How will your idea solve this problem?</strong> </td><td>" . strip_tags($form_elements['how_idea_solve']) . "</td></tr>";
                            $message .= "<tr><td><strong>Who is/are your target customer(s)?</strong> </td><td>" . strip_tags($form_elements['target_customer']) . "</td></tr>";
                            $message .= "</table>";
                            $message .= "</body></html>";
                            $message .= '<html><body>';




                            // FRENCH MESSAGE
                            $message_french = '<html><body>';
                            $message_french .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                            //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                            $message_french .= "<tr><td><strong>Nom et Prénom:</strong> </td><td>" . strip_tags($form_elements['full_name']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Date de naissance:</strong> </td><td>" . strip_tags($form_elements['birthdate']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Adresse :</strong> </td><td>" . strip_tags($form_elements['address']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Numéro de téléphone</strong> </td><td>" . strip_tags($form_elements['phone_number']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Email</strong> </td><td>" . strip_tags($form_elements['email']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Formation scolaire</strong> </td><td>" . strip_tags($form_elements['educational_background']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Emploi actuel/poste/études</strong> </td><td>" . strip_tags($form_elements['current_job']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Twitter profil</strong> </td><td>" . strip_tags($form_elements['twitter_profile']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Facebook profil</strong> </td><td>" . strip_tags($form_elements['facebook_profile']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Langues (quelles langues vous maîtrisez):</strong> </td><td>" . $language_skills . "</td></tr>";
                            $message_french .= "<tr><td><strong>Autre ldangue: </strong> </td><td>" . strip_tags($form_elements['other_language']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Member type</strong> </td><td>Core member</td></tr>";
                            //$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                            //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                            $message_french .= "<tr><td><strong>Nom de l'équipe:</strong> </td><td>" . strip_tags($form_elements['full_name']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Could you give us a short summary or your previous experiences with having a business/setting up a start-up (use max. 300 words)?</strong> </td><td>" . strip_tags($form_elements['experience']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Quelle est votre motivation pour participer à ce programme? </strong> </td><td>" . strip_tags($form_elements['motivation']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Quelles sont vos attentes par rapport à ce programme?</strong> </td><td>" . strip_tags($form_elements['hope_to_gain']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Avez-vous déjà participé à un programme d'accélération ou à une formation sur l’entrepreneuriat?</strong> </td><td>" . strip_tags($form_elements['other_participations']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Si oui, le(s)quel(s) ?</strong> </td><td>" . strip_tags($form_elements['wich_participations']) . "</td></tr>";
                            //$message .= "<tr><td><strong>Have you ever run a start-up or business?</strong> </td><td>" . strip_tags($form_elements['run_start_up']) . "</td></tr>";
                            //$message .= "<tr><td><strong>If so how many?</strong> </td><td>" . strip_tags($form_elements['how_many_start_up']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Gérez-vous une start-up actuellement ?</strong> </td><td>" . strip_tags($form_elements['current_start_up']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Si oui, quel est le nom de votre entreprise ?</strong> </td><td>" . strip_tags($form_elements['name_your_business']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Êtes-vous le fondateur/ co-fondateur de cette entreprise ?</strong> </td><td>" . strip_tags($form_elements['founder_cofounder']) . "</td></tr>";

                            $message_french .= "<tr><td><strong>Quelle est votre idée pour ce programme?</strong> </td><td>" . strip_tags($form_elements['idea_description']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Quel est le stade de la mise en oeuvre de cette idée? </strong> </td><td>" . strip_tags($form_elements['how_far']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Quel est le problème que vous tentez résoudre avec cette idée ?</strong> </td><td>" . strip_tags($form_elements['problem_to_solve']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Comment allez-vous résoudre ce problème avec l’idée ?</strong> </td><td>" . strip_tags($form_elements['how_idea_solve']) . "</td></tr>";
                            $message_french .= "<tr><td><strong>Qui sont vos clients cibles ? </strong> </td><td>" . strip_tags($form_elements['target_customer']) . "</td></tr>";
                            $message_french .= "</table>";
                            $message_french .= "</body></html>";
                            $message_french .= '<html><body>';

                            // GET THE ID FROM THE TEAM

                            global $wpdb;
                            $team_table = $wpdb->prefix . "reg_table_teams";
                            $team = $wpdb->get_row("SELECT * FROM $team_table ORDER BY id DESC");
                            $team_id = $team->id;


                            // EMAIL TO ADMIN
                            $from_email = "kevcoy9@gmail.com";
                            $this->view->sendEmail($from_email, get_option('_nrRegEm'), 'A new team registration', $message);

                            // EMAIL TO TEAM HEADER
                            $message = "Dear applicant,<br>
                            Thank you for applying for the Disrupt!Media! event, below you will find your answers which you filled out in the form.<br>
                            You will receive a response regarding our final decision on September 19th.<br>
                            If you have any questions please contact Kevin Coyne – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a>.<br>" . $message;

                            $message_french = "Chèr(e) candidat(e),<br>
                            Merci de votre inscription à l'évènement Disrupt!/Media!/. Veuillez trouver ci-dessous les réponses que vous avez soumis dans le formulaire.<br>
                            Vous recevrez une réponse concernant notre décision finale d’ici le 19 septembre.<br>
                            Pour toute question, merci de contacter Kevin Coyne par email – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a>.<br> " . $message_french;
                            // EMAIL TO USER
                            if ($form_elements['language_traslation'] == "english") {
                                $from_email = "kevcoy9@gmail.com";
                                $this->view->sendEmail($from_email, $form_elements['email'], 'Your registration', $message);

                                $thanks_message = '<div id="persInfo">Thank you for registering for Disrupt!/Media!. You will receive a confirmation in your email. For questions please contact us through <a href="kevcoy9@gmail.com">kevcoy9@gmail.com</a><br></div>';
                            }

                            if ($form_elements['language_traslation'] == "french") {
                                $from_email = "kevcoy9@gmail.com";
                                $this->view->sendEmail($from_email, $form_elements['email'], 'Votre inscription', $message_french);

                                $thanks_message = "<div id='persInfo'>Chèr(e) candidat(e),<br>
                            Merci de votre inscription à l'évènement Disrupt!/Media!/. Veuillez trouver ci-dessous les réponses que vous avez soumis dans le formulaire.<br>
                            Vous recevrez une réponse concernant notre décision finale d’ici le 19 septembre.<br>
                            Pour toute question, merci de contacter Kevin Coyne par email – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a><br></div>";
                            }


                            $number_members = $form_elements['number_members'];
                            if ($number_members > 1) {
                                $data = array();
                                $var = array('team_id' => $team_id, 'number_members' => $number_members, 'team_name' => $form_elements['full_name'], 'team_email' => $form_elements['email'], 'language_traslation' => $form_elements['language_traslation']);
                                $this->view->loadView('system/views/frontend/member_register', $var);
                                //wp_redirect( home_url() . '/team-member/?team_id=' . $team_id . '&number_members=' . $number_members );
//                                $data = array('success' => 'Nice you’re joining us! If you are unable to attend in the end, could you let us know at <a href="mailto:' . get_option('_nrRegEm') . '">' . get_option('_nrRegEm') . '</a> ?.'
//                                    . '<br>'
//                                    . 'If you want add a team member, <a href="' . home_url() . '/team-member/?team_id=' . $team_id . '&number_members=' . $number_members . '">press here</a>');
                            } else {
                                $data = array('success' => $thanks_message,
                                    "errorElements" => null);
                            }
                        } else {
                            $this->view->logData("Error occurred in normal registration process");
                        }
                    } else {
                        $data = array(
                            'errorElements' => $result,
                            'scrollTo' => '#errors',
                        );
                    }
                    if (!empty($data)) {
                        echo json_encode($data);
                    }
                }









                if (isset($form_elements['member_register'])) {

                    $this->view->logData("User start normal member process");

                    $result = array();
                    $error = 0;

                    $data = null;
                    $number_members = $form_elements['number_members'];
                    for ($i = 1; $i < $number_members; $i++) {
                        $data = null;
                        $full_name = "full_name" . $i;
                        $birth_date = "birthdate" . $i;
                        $address = "address" . $i;
                        $phone_number = "phone_number" . $i;
                        $email = "email" . $i;
                        $educational_background = "educational_background" . $i;
                        $current_position = "current_position" . $i;
                        $twitter_profile = "twitter_profile" . $i;
                        $facebook_profile = "facebook_profile" . $i;
                        $language_skills = "language_skills" . $i;
                        $other_language = "other_language" . $i;
                        $member_type = "member_type" . $i;

                        if (isset($form_elements[$language_skills])) {
                            $language_skills_values = implode(",", $form_elements[$language_skills]);
                        } else {
                            $language_skills_values = "";
                        }


                        // $language_skills_value = implode(",", $form_elements[$language_skills]);

                        if (empty($form_elements[$full_name])) {
                            $error = 1;

                            array_push($result, array('label' => $full_name, "message" => "Full name", "message_french" => "Nom et prénom"));

                            //$result = array("full_name" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: full_name");
                        }
                        if (empty($form_elements[$birth_date])) {
                            $error = 1;

                            array_push($result, array('label' => $birth_date, "message" => "Birth date", "message_french" => "Date de naissance"));

                            // $result = array("birthdate" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: birthdate");
                        }


                        if (empty($form_elements[$email])) {
                            $error = 1;

                            //$result = array("email" => ' *Required ');
                            array_push($result, array('label' => $email, "message" => "Email", "message_french" => "Email"));

                            $this->view->logData("User tried to register with empty field: email");
                        }

                        if (empty($form_elements[$educational_background])) {
                            $error = 1;
                            array_push($result, array('label' => $educational_background, "message" => "Educational background", "message_french" => "Formation scolaire"));


                            //$result = array("educational_background" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: educational_background");
                        }
                        if (empty($form_elements[$current_position])) {
                            $error = 1;

                            array_push($result, array('label' => $current_position, "message" => "Current job/position/study", "message_french" => "Emploi actuel/poste/études"));

                            //$result = array("current_job" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: current_job");
                        }
                        if (empty($form_elements[$member_type])) {
                            $error = 1;

                            array_push($result, array('label' => $member_type, "message" => "Position within the team", "message_french" => "Rôle dans l’équipe"));

                            //$result = array("current_job" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: current_job");
                        }
                        //if (empty($form_elements[$language_skills]) && empty($form_elements[$other_language])) {
                        if (!isset($form_elements[$language_skills])) {
                            $error = 1;
                            array_push($result, array('label' => $language_skills, "message" => "Language skills (which languages do you speak/understand)", "message_french" => "Langues (quelles langues vous maîtrisez)"));

                            // $result = array("language_skills" => ' *Required ');
                            $this->view->logData("User tried to register with empty field: language_skills");
                        }
                    }
                    if ($error == 0) {

                        if ($this->register->addNormalRegisterMemberItem($form_elements)) {
                            $this->view->logData("Normal item successfully registered");
                            for ($i = 1; $i < $number_members; $i++) {
                                $full_name = "full_name" . $i;
                                $birth_date = "birthdate" . $i;
                                $address = "address" . $i;
                                $phone_number = "phone_number" . $i;
                                $email = "email" . $i;
                                $educational_background = "educational_background" . $i;
                                $current_position = "current_position" . $i;
                                $twitter_profile = "twitter_profile" . $i;
                                $facebook_profile = "facebook_profile" . $i;
                                $language_skills = "language_skills" . $i;
                                $other_language = "other_language" . $i;
                                $member_type = "member_type" . $i;


                                if (isset($form_elements[$language_skills])) {
                                    $language_skills_value = implode(",", $form_elements[$language_skills]);
                                } else {
                                    $language_skills_value = "";
                                }

                                // $language_skills_value = implode(",", $form_elements[$language_skills]);
                                $message = '<html><body>';
                                $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                                //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                                $message .= "<tr><td><strong>Team name:</strong> </td><td>" . strip_tags($form_elements['team_name']) . "</td></tr>";
                                $message .= "<tr><td><strong>Full name:</strong> </td><td>" . strip_tags($form_elements[$full_name]) . "</td></tr>";
                                $message .= "<tr><td><strong>Birth date:</strong> </td><td>" . strip_tags($form_elements[$birth_date]) . "</td></tr>";
                                $message .= "<tr><td><strong>Email</strong> </td><td>" . strip_tags($form_elements[$email]) . "</td></tr>";
                                $message .= "<tr><td><strong>Educational background</strong> </td><td>" . strip_tags($form_elements[$educational_background]) . "</td></tr>";
                                $message .= "<tr><td><strong>Current job/position/study</strong> </td><td>" . strip_tags($form_elements[$current_position]) . "</td></tr>";
                                $message .= "<tr><td><strong>Twitter profile </strong> </td><td>" . strip_tags($form_elements[$twitter_profile]) . "</td></tr>";
                                $message .= "<tr><td><strong>Facebook profile </strong> </td><td>" . strip_tags($form_elements[$facebook_profile]) . "</td></tr>";

                                $message .= "<tr><td><strong> Language skills (which languages do you speak/understand): </strong> </td><td>" . $language_skills_value . "</td></tr>";
                                $message .= "<tr><td><strong> Other language: </strong> </td><td>" . strip_tags($form_elements[$other_language]) . "</td></tr>";
                                //$message .= "<tr><td><strong>Member type</strong> </td><td>" . strip_tags($form_elements[$member_type]) . "</td></tr>";




                                $message .= "</table>";
                                $message .= "</body></html>";






                                // FRENCH MESSAGE
                                $message_french = '<html><body>';
                                $message_french .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                                //$message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
                                $message_french .= "<tr><td><strong>Nom de l'équipe:</strong> </td><td>" . strip_tags($form_elements[$full_name]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Nom et Prénom:</strong> </td><td>" . strip_tags($form_elements[$full_name]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Date de naissance:</strong> </td><td>" . strip_tags($form_elements[$birth_date]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Email</strong> </td><td>" . strip_tags($form_elements[$email]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Formation scolaire</strong> </td><td>" . strip_tags($form_elements[$educational_background]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Emploi actuel/poste/études</strong> </td><td>" . strip_tags($form_elements[$current_position]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Twitter profil</strong> </td><td>" . strip_tags($form_elements[$twitter_profile]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Facebook profil</strong> </td><td>" . strip_tags($form_elements[$facebook_profile]) . "</td></tr>";
                                $message_french .= "<tr><td><strong>Langues (quelles langues vous maîtrisez):</strong> </td><td>" . $language_skills_value . "</td></tr>";
                                $message_french .= "<tr><td><strong>Autre ldangue: </strong> </td><td>" . strip_tags($form_elements[$other_language]) . "</td></tr>";

                                $message_french .= "</table>";
                                $message_french .= "</body></html>";
                                $message_french .= '<html><body>';

                                // EMAIL FOR ADMIN
                                $from_email = "kevcoy9@gmail.com";
                                $this->view->sendEmail($from_email, get_option('_nrRegEm'), 'A new member team registration', $message);

                                // EMAIL FOR MEMBER
                                $message = "Dear applicant,<br>
                                Thank you for applying for the Disrupt!Media! event, below you will find your answers which you filled out in the form.<br>
                                You will receive a response regarding our final decision on September 19th.<br>
                                If you have any questions please contact Kevin Coyne – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a>.<br>" . $message;


                                $message_french = "Chèr(e) candidat(e),<br>
                            Merci de votre inscription à l'évènement Disrupt!/Media!/. Veuillez trouver ci-dessous les réponses que vous avez soumis dans le formulaire.<br>
                            Vous recevrez une réponse concernant notre décision finale d’ici le 19 septembre.<br>
                            Pour toute question, merci de contacter Kevin Coyne par email – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a>.<br> " . $message_french;


                                // EMAIL TO USER
                                if ($form_elements['language_traslation'] == "english") {
                                    $from_email = "kevcoy9@gmail.com";
                                    $this->view->sendEmail($from_email, $form_elements['team_email'], 'Your registration', $message);

                                    $thanks_message = '<div id="persInfo">Thank you for registering for Disrupt!/Media!. You will receive a confirmation in your email. For questions please contact us through <a href="kevcoy9@gmail.com">kevcoy9@gmail.com</a><br></div>';
                                }

                                if ($form_elements['language_traslation'] == "french") {
                                    $from_email = "kevcoy9@gmail.com";
                                    $this->view->sendEmail($from_email, $form_elements['team_email'], 'Votre inscription', $message_french);

                                    $thanks_message = "<div id='persInfo'>Chèr(e) candidat(e),<br>
                            Merci de votre inscription à l'évènement Disrupt!/Media!/. Veuillez trouver ci-dessous les réponses que vous avez soumis dans le formulaire.<br>
                            Vous recevrez une réponse concernant notre décision finale d’ici le 19 septembre.<br>
                            Pour toute question, merci de contacter Kevin Coyne par email – <a href='mailto:kevcoy9@gmail.com'>kevcoy9@gmail.com</a><br></div>";
                                }



                                // $this->view->sendEmail($form_elements["team_email"], 'Your registration - Member', $message);
                            }

                            // GET THE ID FROM THE TEAM
//                                $team_id = $form_elements['team_id'];


                            $data = array('success' => $thanks_message);
//                            wp_die("A");
                        } else {
                            $this->view->logData("Error occurred in normal registration process");
                        }
                    } else {
                        $data = array(
                            'errorElements' => $result,
                            'scrollTo' => '#errors',
                        );
//                        wp_die("B");
                    }

                    echo json_encode($data);
                }
            endif;
            wp_die();
        }

        /*         * *
         * Register menu and submenus
         * @access	public
         * @return	void
         */

        public function MECRegisterMenu() {
            //Add menu
            $create_theme_hook = add_menu_page(
                    'Disrupt', 'Disrupt', 'add_users', 'register-home', array($this, 'MEC_webberty_home')
            );

            //Add Scripts only in Debaser New Theme page
            add_action('admin_print_scripts-' . $create_theme_hook, array($this, 'MEC_create_theme_admin_scripts'));

            //Add Styles only in Debaser New Theme page
            add_action('admin_print_styles-' . $create_theme_hook, array($this, 'MEC_create_theme_admin_styles'));

            //Add submenu
//            $create_payment_register_theme_hook = add_submenu_page(
//                'register-home',
//                'Pay Register Form',
//                'Pay Register Form',
//                'edit_pages',
//                'register-payment',
//                array($this,'MEC_webberty_payment_register_view')
//            );
            //Add Scripts only in Debaser New Theme page
//            add_action('admin_print_scripts-' . $create_payment_register_theme_hook, array($this, 'MEC_create_theme_admin_scripts'));
            //Add Styles only in Debaser New Theme page
//            add_action('admin_print_styles-' . $create_payment_register_theme_hook, array($this, 'MEC_create_theme_admin_styles'));
            //Add submenu
            $create_normal_register_theme_hook = add_submenu_page(
                    'register-home', 'Export', 'Export', 'edit_pages', 'register-normal', array($this, 'MEC_webberty_normal_register_view')
            );
            //Add Scripts only in Debaser New Theme page
            add_action('admin_print_scripts-' . $create_normal_register_theme_hook, array($this, 'MEC_create_theme_admin_scripts'));

            //Add Styles only in Debaser New Theme page
            add_action('admin_print_styles-' . $create_normal_register_theme_hook, array($this, 'MEC_create_theme_admin_styles'));


            //Add submenu
            $create_settings_theme_hook = add_submenu_page(
                    'register-home', 'Settings', 'Settings', 'edit_pages', 'register-settings', array($this, 'MEC_webberty_settings')
            );
        }

        /*         * *
         * Load Payments records and load individual item
         * @access	public
         * @return	string
         */

        public function MEC_webberty_payment_register_view() {

            $get = $this->input->get();

            if (isset($get['view_payment']) && $this->register->getRegisterItem('payment', $get['view_payment'])) {
                $var = array(
                    'page_title' => ' Payment Register.',
                    'get_register_item' => $this->register->getRegisterItem('payment', $get['view_payment'])
                );

                $this->register->markItemAsReaded('payment', $get['view_payment']);
                $this->view->loadView('system/views/backend/pay_register_form/view-item', $var);
            } else {

                $var = array(
                    'page_title' => ' Payment Register.',
                    'normal_register_list' => $this->register->getPaymentRegister()
                );

                $this->view->loadView('system/views/backend/pay_register_form/view', $var);
            }
        }

        /*         * *
         * Load Normal records and load individual item
         * @access	public
         * @return	string
         */

        public function MEC_webberty_normal_register_view() {
            $get = $this->input->get();

            if (isset($get['view_normal']) && $this->register->getRegisterItem('normal', $get['view_normal'])) {
                $var = array(
                    'page_title' => 'Normal Register.',
                    'get_register_item' => $this->register->getRegisterItem('normal', $get['view_normal'])
                );

                $markItem = $this->register->markItemAsReaded('normal', $get['view_normal']);
                $this->view->loadView('system/views/backend/normal_register_form/view-item', $var);
            } else {

                $var = array(
                    'page_title' => ' Register.',
                    'normal_register_list' => $this->register->getNormalRegister()
                );

                $this->view->loadView('system/views/backend/normal_register_form/view', $var);
            }
        }

        //TODO
        public function MEC_webberty_settings() {
            $data = array(
                'page_title' => 'Register Settings.',
            );

            $this->view->loadView('system/views/backend/settings/view', $data);
        }

        public function MEC_ajax_save_settings() {

            $post = $this->input->post();

//            if(isset($post['payMode']))
//            {
//                update_option( '_payMode', $post['payMode'] );
//            }
//            if(isset($post['payCurrency']))
//            {
//                update_option( '_payCurrency', $post['payCurrency'] );
//            }
//            if(isset($post['payCulture']))
//            {
//                update_option( '_payCulture', $post['payCulture'] );
//            }
//
//            if(isset($post['payAccId']))
//            {
//                update_option( '_payAccId', $post['payAccId'] );
//            }
//            if(isset($post['paySiteId']))
//            {
//                update_option( '_paySiteId', $post['paySiteId'] );
//            }

            if (isset($post['nrRegEm'])) {
                update_option('_nrRegEm', $post['nrRegEm']);
            }
//            if(isset($post['paySiteCode']))
//            {
//                update_option( '_paySiteCode', $post['paySiteCode'] );
//            }

            echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>Settings saved.</strong></p></div>';

            wp_die();
        }

        //TODO
        public function MEC_webberty_home() {

            $var = array(
                'page_title' => ' Dashboard.',
                'normal_register_total' => count($this->register->getNormalRegister()),
                'normal_register_new' => $this->register->countRegisters('normal'),
//                'payment_register_new'      =>  $this->register->countRegisters('payment'),
//                'payment_register_total'    => count($this->register->getPaymentRegister()),
            );


            $create_normal_register_theme_hook = add_submenu_page(
                    'register-home', 'Normal Register Form', 'Normal Register Form', 'edit_pages', 'register-normal', array($this, 'MEC_webberty_normal_register_view')
            );
            $this->view->loadView('system/views/backend/home/view', $var);
        }

        /*         * *
         * Load scripts in admin area
         * @access	public
         * @return	void
         */

        public function MEC_create_theme_admin_scripts() {
            /* Link our already registered script to a page */
            wp_enqueue_script(
                    'bootstrap-js', plugins_url('webberty_form/system/assets/js/bootstrap.js'), array('jquery'), '2.3.2', true
            );

            /* Link our already registered script to a page */
            wp_enqueue_script(
                    'bootstrap-tooltip-js', plugins_url('webberty_form/system/assets/js/bootstrap-tooltip.js'), array('jquery', 'bootstrap-js'), '2.3.2', true
            );

            wp_enqueue_script(
                    'textareacounrter-js', plugins_url('webberty_form/system/assets/js/jquery.textareaCounter.plugin.js'), array('jquery'), '2.3.2'
            );

            wp_enqueue_script(
                    'main-admin-js', plugins_url('webberty_form/system/assets/js/main-admin.js'), array('jquery'), '2.3.2'
            );


            wp_localize_script('main-admin-js', 'MyAjax', array(
                'ajaxurl' => admin_url('admin-ajax.php')
                , 'home' => home_url()
                    )
            );
        }

        /*         * *
         * Load styles in admin area
         * @access	public
         * @return	void
         */

        public function MEC_create_theme_admin_styles() {
            /* Link our already registered script to a page */
            wp_enqueue_style(
                    'bootstrap-css', plugins_url('webberty_form/system/assets/css/bootstrap.css'), '2.3.2'
            );

            wp_enqueue_style(
                    'bootstrap-responsive-css', plugins_url('webberty_form/system/assets/css/bootstrap-responsive.css'), '2.3.2'
            );

            wp_enqueue_style(
                    'prettify-css', plugins_url('webberty_form/system/assets/css/prettify.css')
            );
        }

        /*         * *
         * Load styles in frontend area in register page
         * @access	public
         * @return	void
         */

        public function add_theme_styles() {

            if (is_page('register') || is_page("team-member")) {
                // Register the style like this for a theme:
                // (First the unique name for the style (custom-style) then the src,
                // then dependencies and ver no. and media type)
//                wp_enqueue_style(
//                        'frontend-style-css', plugins_url('webberty_form/system/assets/css/frontend-style.css')
//                );

                wp_enqueue_style(
                        'jquery-ui-css', 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css'
                );
            }
        }

        /*         * *
         * Load scripts in frontend area in register page
         * @access	public
         * @return	void
         */

        public function add_theme_scripts() {

            if (is_page('register') || is_page("team-member")) {
                /* Link our already registered script to a page */
                wp_enqueue_script(
                        'bootstrap-tab-js', plugins_url('webberty_form/system/assets/js/bootstrap-tab.js'), array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'jquery-ui-js', 'http://code.jquery.com/ui/1.10.3/jquery-ui.js', array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'main-frontend-js', plugins_url('webberty_form/system/assets/js/main-frontend.js'), array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'reg1-js', plugins_url('webberty_form/system/assets/js/reg1.js'), array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'scrollTo-js', plugins_url('webberty_form/system/assets/js/jquery.scrollTo-min.js'), array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'jquery-observehashchange-js', plugins_url('webberty_form/system/assets/js/jquery.observehashchange.js'), array('jquery'), '2.3.2', true
                );

                wp_enqueue_script(
                        'simplyCountable-js', plugins_url('webberty_form/system/assets/js/jquery.simplyCountable.js'), array('jquery'), '2.3.2', true
                );

                wp_localize_script('main-frontend-js', 'MyAjax', array(
                    'ajaxurl' => admin_url('admin-ajax.php')
                    , 'home' => home_url()
                        )
                );
            }

            if (is_admin()) {

                wp_enqueue_script(
                        'main-admin-js', plugins_url('webberty_form/system/assets/js/main-admin.js'), array('jquery'), '2.3.2'
                );
                wp_localize_script('main-admin-js', 'MyAjax', array(
                    'ajaxurl' => admin_url('admin-ajax.php')
                    , 'home' => home_url()
                        )
                );
            }
        }

        /**
         * Send payment registration email model
         * @access	private
         * @param $payment_id
         * @param $admin_email
         * @return mixed
         */
        private function __MEC_payment_email_template($payment_id, $admin_email = null) {
            $payment_data = $this->register->getRegisterItem('payment', $payment_id);

            $po = 0;
            if ($payment_data->total == '90.75') {
                $po = '75';
            } elseif ($payment_data->total == '242') {
                $po = '200';
            } elseif ($payment_data->total == "363") {
                $po = '300';
            } elseif ($payment_data->total == "0") {
                $po = '0';
            }

            $message = '<html><body>';
            $message .= "<p> Thank you very much for your registration, we look forward seeing you at Open for Change! Our programme team will contact you with regards to logistics (visa, transport, reimbursements, etc). If you have any questions, please take a look at our Frequently Asked Questions, or e-mail us at " . get_option("_nrRegEm") . "</p>";
            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
            $message .= "<tr style='background: #eee;'><td><strong>Field Name:</strong> </td><td>Value</td></tr>";
            $message .= "<tr><td><strong>Name:</strong> </td><td>" . strip_tags($payment_data->your_name_4) . "</td></tr>";
            $message .= "<tr><td><strong>Family Name:</strong> </td><td>" . strip_tags($payment_data->your_fam_name_4) . "</td></tr>";
            $message .= "<tr><td><strong>Gender:</strong> </td><td>" . strip_tags($payment_data->gender_option_4) . "</td></tr>";
            $message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($payment_data->your_email_4) . "</td></tr>";
            $message .= "<tr><td><strong>Mobile Phone:</strong> </td><td>" . strip_tags($payment_data->mobile_phone_4) . "</td></tr>";
            $message .= "<tr><td><strong>Street:</strong> </td><td>" . strip_tags($payment_data->input_street_4) . "</td></tr>";
            $message .= "<tr><td><strong>City:</strong> </td><td>" . strip_tags($payment_data->input_city_4) . "</td></tr>";
            $message .= "<tr><td><strong>ZIP Code:</strong> </td><td>" . strip_tags($payment_data->input_zip_code_4) . "</td></tr>";
            $message .= "<tr><td><strong>Nationality:</strong> </td><td>" . strip_tags($payment_data->input_nationality_4) . "</td></tr>";
            $message .= "<tr><td><strong>Affiliated with an organization:</strong></td><td>" . strip_tags($payment_data->input_affiliated_organization_4) . "</td></tr>";
            $message .= "<tr><td><strong>Street organization:</strong> </td><td>" . strip_tags($payment_data->input_org_street_4) . "</td></tr>";
            $message .= "<tr><td><strong>Organization Zip code:</strong> </td><td>" . strip_tags($payment_data->input_org_zip_code_4) . "</td></tr>";
            $message .= "<tr><td><strong>Organization City:</strong> </td><td>" . strip_tags($payment_data->input_org_city_4) . "</td></tr>";
            $message .= "<tr><td><strong>Organization Country:</strong> </td><td>" . strip_tags($payment_data->input_org_country_4) . "</td></tr>";
            $message .= "<tr><td><strong>Organization Mobile Phone:</strong> </td><td>" . strip_tags($payment_data->input_org_mobile_phone_4) . "</td></tr>";
            $message .= "<tr><td><strong>Emergency Name</strong> </td><td>" . strip_tags($payment_data->case_emergency_name_4) . "</td></tr>";
            $message .= "<tr><td><strong>Emergency Name</strong> </td><td>" . strip_tags($payment_data->case_emergency_name_4) . "</td></tr>";
            $message .= "<tr><td><strong>Relationship / Affiliation</strong> </td><td>" . strip_tags($payment_data->case_emergency_relationship_4) . "</td></tr>";
            $message .= "<tr><td><strong>Emergency Phone</strong> </td><td>" . strip_tags($payment_data->case_emergency_phone_4) . "</td></tr>";
            $message .= "<tr><td><strong>Dietary requirements?</strong></td><td>";
            $dietary_requirements_3 = unserialize($payment_data->dietary_requirements_4);
            if (empty($dietary_requirements_3)) {
                $message .="<p>---</p>";
            } else {
                foreach ($dietary_requirements_3 as $dietary_requirement_3) {
                    $message .="<p>" . $dietary_requirement_3 . "</p>";
                }
            }
            $message .= "</td></tr>";

            $message .= "<tr><td><strong>Other, namely?</strong> </td><td>" . strip_tags($payment_data->other_namely_4) . "</td></tr>";

            $message .= "<tr><td><strong>You hear from this event...</strong> </td><td>";
            $hear_from_this_event = unserialize($payment_data->hear_from_this_event);

            if (empty($hear_from_this_event)) {
                $message .="<p>---</p>";
            } else {
                foreach ($hear_from_this_event as $hear_from_this_event_3) {
                    $message .="<p>" . $hear_from_this_event_3 . "</p>";
                }
            }

            $message .= "</td></tr>";

            $message .= "<tr><td><strong>Food allergies ?</strong> </td><td>" . strip_tags($payment_data->food_allergies_4) . "</td></tr>";
            $message .= "<tr><td><strong>Special assistance?</strong> </td><td>" . strip_tags($payment_data->special_assistance_4) . "</td></tr>";
            $message .= "<tr><td><strong>Medical condition?</strong> </td><td>" . strip_tags($payment_data->medical_condition_4) . "</td></tr>";
            $message .= "<tr><td><strong>Do you live outside The Netherlands?</strong> </td><td>" . strip_tags($payment_data->live_outside_netherlands_4 == 0 ? ' No ' : ' Yes ') . "</td></tr>";
            $message .= "<tr><td><strong>Need a visa to visit the EU?</strong> </td><td>" . strip_tags($payment_data->need_visa_to_visit_eu == 0 ? ' No ' : ' Yes ') . "</td></tr>";


            $message .= "<tr><td><strong>Your personal passport number</strong> </td><td>" . strip_tags($payment_data->inputPassN) . "</td></tr>";
            $message .= "<tr><td><strong>Issued on</strong> </td><td>" . strip_tags($payment_data->inputIssueOn) . "</td></tr>";
            $message .= "<tr><td><strong>Place of issue</strong> </td><td>" . strip_tags($payment_data->inputIssuePl) . "</td></tr>";
            $message .= "<tr><td><strong>Expires</strong> </td><td>" . strip_tags($payment_data->inputPassNExpires) . "</td></tr>";
            $message .= "<tr><td><strong>Your place of birth</strong> </td><td>" . strip_tags($payment_data->inputPlaceOfBirth) . "</td></tr>";




            $message .= "<tr><td><strong>Date of arrival</strong> </td><td>" . strip_tags($payment_data->date_of_arrival) . "</td></tr>";
            $message .= "<tr><td><strong>Date of departure</strong> </td><td>" . strip_tags($payment_data->date_of_departure) . "</td></tr>";
            $message .= "<tr><td><strong>Payment Option</strong> </td><td>" . strip_tags($po) . "</td></tr>";
            $message .= "<tr><td><strong>Payment Status</strong> </td><td>" . strip_tags($payment_data->payment_status) . "</td></tr>";
            $message .= "<tr><td><strong>Total</strong> </td><td>" . strip_tags($payment_data->total) . "</td></tr>";


            $message .= "</table>";
            $message .= "</body></html>";
            if ($admin_email) {
                //$this->view->sendEmail($admin_email, 'Thank you very much for your registration', $message);
            } else {
                //$this->view->sendEmail($payment_data->your_email_4, 'Thank you very much for your registration', $message);
            }
        }

        /*         * *
         * The Short Code function
         * @access	public
         * @return	string
         */

        public function MEC_webberty_form_short_code() {
            $get = $this->input->get();

//            if(isset($get['request']) && $get['request'] == '3')
//            {
            $var = array('page_title' => ' Settings.');

            $this->view->loadView('system/views/frontend/form_register_3', $var);
//            }
        }

        public function MEC_webberty_team_member_short_code() {
            $get = $this->input->get();

//            if(isset($get['request']) && $get['request'] == '3')
//            {
            $var = array('page_title' => ' Settings.');

            $this->view->loadView('system/views/frontend/member_register', $var);
//            }
        }

        public function export_normal() {
            if ($this->input->is_ajax_request()):

                $post = $this->input->post();

                if ($post['edit_item'] == 'all') {

                    $item = $this->register->getNormalRegister();

                    if (!empty($item)) {
                        $data = array();
                        //Header Table

                        $data[] = array(
                            "Team",
                            "Team Name",
                            "Birth date",
                            "Address",
                            "Phone number",
                            "Email",
                            "Educational background",
                            "Current position",
                            "Twitter profile",
                            "Facebook profile",
                            "Language skills",
                            "Other language",
                            "Member type",
                            "Idea description",
                            "How far are you with implementing your idea",
                            "What problem you are attempting to solve with your idea?",
                            "How will your idea solve this problem?",
                            "Who is/are your target customer(s)?",
                            "Short summary",
                            "What is your motivation to join this training programme? ",
                            "What do you hope to gain from this training?",
                            "Have you ever attended or participated in acceleration programmes or entrepreneurship related trainings?",
                            "If so which one(s):",
                            "Have you ever run a start-up or business?",
                            "If so how many?",
                            "Are you currently running a start-up/business?",
                            "What is the name of your business (if applicable)?",
                            "Are you the founder/cofounder of that business (if applicable)?",
                           
                        );
                        foreach ($item as $itm) {

//                            $data[] = array(
//                                        "Team",        
//                                    );
                            // AQUI PONEMOS LOS NOMBRES DE LAS COLUMNAS DE LOS TEAM MEMBERS

                            $members = $this->register->getTeamMembers($itm->id);
                            if (!empty($members)) {


                                foreach ($members as $member) {
                                     if ($member->member_type == "Head") {
                                    $data[] = array(
                                        $itm->id,
                                        $member->full_name,
                                        $member->birth_date,
                                        $member->address,
                                        $member->phone_number,
                                        $member->email,
                                        $member->educational_background,
                                        $member->current_position,
                                        $member->twitter_profile,
                                        $member->facebook_profile,
                                        $member->language_skills,
                                        $member->other_language,
                                        $member->member_type,
                                         $itm->idea_description,
                                            $itm->how_far,
                                            $itm->problem_to_solve,
                                            $itm->how_idea_solve,
                                            $itm->target_customer,
                                            $itm->experience,
                                            $itm->motivation,
                                            $itm->hope_to_gain,
                                            $itm->other_participations,
                                            $itm->wich_participations,
                                            $itm->run_start_up,
                                            $itm->how_many_start_up,
                                            $itm->current_start_up,
                                            $itm->name_your_business,
                                            $itm->founder_cofounder,
                                    );
                                   
                                    }
                                    else{
                                        $data[] = array(
                                            $itm->id,
                                        $member->full_name,
                                        $member->birth_date,
                                        $member->address,
                                        $member->phone_number,
                                        $member->email,
                                        $member->educational_background,
                                        $member->current_position,
                                        $member->twitter_profile,
                                        $member->facebook_profile,
                                        $member->language_skills,
                                        $member->other_language,
                                        $member->member_type, 
                                           "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
                                            "",
//                                        $member->id_team,
                                        );
                                    }
                                }
                            }
//                            $data[] = array(
//                                        "",        
//                                    );
                        }

                        $list = $data;
                    }
                }

                if (!empty($list)) {
                    // file name for download
                    $filename = "normal_register_" . date('Ymd') . ".xls";

                    header("Content-Disposition: attachment; filename=\"$filename\"");
                    header("Content-Type: application/vnd.ms-excel");
                    $fp = fopen(ABSPATH . "wp-content/plugins/webberty_form/" . $filename, 'w+');

                    foreach ($list as $fields) {
                        //$fields = $fields . "\n";
                        fputcsv($fp, $fields);
                    }

                    fclose($fp);



                    $link = get_site_url() . "/wp-content/plugins/webberty_form/" . $filename;
                    echo $link;
                }

            //print_R($list) ;

            endif;

            wp_die();
        }

    }

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

    

endif;