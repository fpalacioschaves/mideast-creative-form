<?php if ( ! defined( 'WPINC' ) ){ die;}
/*
Plugin Name: Mideast register form
Plugin URI: http://www.webberty.com
Description: Mideast register form
Version: 2.2
Author: Webberty Team, 
Author URI: Webberty.com
Copyright GPL-2013  Author name(email : adrian@webberty.com)
*/


/************************************************************************************************************************
 * ****************************************MASTER BRANCH****************************************************************
 ***********************************************************************************************************************/


/***
 * Check if our parent class  plugin is declared
 * We run this plugin only for admins
 * */

   
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

    require plugin_dir_path( __FILE__ ) .'plugin-updates/plugin-update-checker.php';
$MyUpdateChecker = PucFactory::buildUpdateChecker(
    'http://wbbupdateserver.bywlab.nl/wp-update-server/webberty_form.json',
    __FILE__,
    'wbb-twitter'
);

    /*Create Tables when plugin is activated*/
    include_once(  ABSPATH. "wp-content/plugins/webberty_form/system/database/create_table.php" );
    register_activation_hook(__FILE__, 'MEC_create_table' );

    //If this plugin is activated then include plugin core files
    if (is_plugin_active('webberty_form/init-plugin.php'))
    {

        //Load all plugin core functions
        require_once("system/config/defined.php");


       
        //Load all plugin core functions
        require_once("system/core/MEC_Core.php");

        //Load all plugin initialization functions
        require_once("system/core/MEC_Core_Init.php");




        //Load everything ones plugin is loaded
        function MEC_plugin_init()
        {
            new MEC_Core_Init();



        }

        add_action( 'plugins_loaded', 'MEC_plugin_init' );

    }



